import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { GalleryViewModel } from './gallery.model';

@Injectable()
export class GalleryController {

    constructor(private httpClient: HttpClient) { }

    public getGalleryByLimit(limit: number): Observable<GalleryViewModel[]> {
        return this.httpClient.get<GalleryViewModel[]>(`/Gallery/GetGalleryByLimit?limit=${limit}`);
    }

    public updateGallery(): Observable<any> {
        return this.httpClient.post("Gallery/UpdateGallery", null) as Observable<any>;
    }

    public authGallery(): Observable<any> {
        return this.httpClient.get(`/Gallery/IndexAsync`) as Observable<any>;
    }
}