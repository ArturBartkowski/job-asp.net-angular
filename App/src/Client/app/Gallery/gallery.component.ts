import { Component } from '@angular/core';
import { GalleryService } from './gallery.service';
import { GalleryViewModel } from './gallery.model';

@Component({
  selector: 'app-gallery-main',
  templateUrl: './gallery.component.html'
})
export class GalleryComponent {

  constructor(private service: GalleryService) {
    this.loadGalleryData(2);
  }
  private gallery: GalleryViewModel[] = [];
  private loaded: boolean = false;
  galleries: string[] = [];

  async loadGalleryData(limit: number) {
    if (this.loaded != true) {
      await this.service.getGalleryByLimit(limit).toPromise().then(res => this.gallery = res)
      this.generateGalleryInnerHTML();
    }
    this.loaded = true;
  }

  generateGalleryInnerHTML() {
    var singlegallery: string = '';

    for (let i = 0; i < this.gallery.length; i++) {
      singlegallery += '<div class="nk-gap-5"></div>'
        + '<h2 class="nk-post-title h3 text-center nk-navbarText">' +
        this.gallery[i].Title + ' ' + new Date(this.getDateFromAspNetFormat(this.gallery[i].EventDate.toString())).toISOString().split('T')[0]
        + '</h2>' +
        '<div class="pa-gallery-player-widget" style="width:100%; height:480px;" data-arrows="true">'
      for (let j = 0; j < this.gallery[i].Images.length; j++) {
        singlegallery += '<img data-src="' + this.gallery[i].Images[j].BaseUrl + "=w1920-h1080"
          + '" src="" alt="" />'
      }
      singlegallery += '</div>'

      this.galleries.push(singlegallery);
      singlegallery = '';
    }
  }

  public getDateFromAspNetFormat(date: string): number {
    const re = /-?\d+/;
    const m = re.exec(date);
    return parseInt(m[0], 10);
  }
}
