import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { GalleryComponent } from './gallery.component';
import { GalleryService } from '../Gallery/gallery.service';
import { GalleryController } from '../Gallery/gallery.controller';
import { NoSanitizePipe } from '../Helpers/nosanitizePipe';

@NgModule({
    imports: [BrowserModule, FormsModule],
    providers: [GalleryService, GalleryController],
    declarations: [GalleryComponent, NoSanitizePipe],
    exports: [GalleryComponent, NoSanitizePipe]
})
export class GalleryModule { }
