import { Injectable } from '@angular/core';
import { GalleryViewModel } from './gallery.model';
import { GalleryController } from './gallery.controller';
import { Observable } from 'rxjs';

@Injectable()
export class GalleryService {

    constructor(private dataSource: GalleryController) { }

    getGalleryByLimit(limit: number): Observable<GalleryViewModel[]> {
        return this.dataSource.getGalleryByLimit(limit);
    }

    requestToUpdateGallery(): Observable<any> {
        return this.dataSource.updateGallery();
    }

    requestToAuthGallery(): Observable<any> {
        return this.dataSource.authGallery();
    }
}