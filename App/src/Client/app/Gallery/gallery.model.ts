import { ImagesViewModel } from './images.model';

export class GalleryViewModel {
    constructor(
        public CreatedBy?: string,
        public EventDate?: Date,
        public Title?: string,
        public Images?: ImagesViewModel[]) { }
}