import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ClubBasicInformationViewModel } from './clubdashboardbasic.model';
import { DashboardController } from './clubdashboard.controller';

@Injectable()
export class DashboardService {

    constructor(private dataSource: DashboardController) { }

    getBasicUserInformation(): Observable<ClubBasicInformationViewModel[]> {
        return this.dataSource.getBasicInformation();
    }
}
