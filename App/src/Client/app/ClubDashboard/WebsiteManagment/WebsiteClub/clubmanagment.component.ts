import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { ClubService } from 'src/Client/app/Club/club.service';
import { ClubViewModel } from 'src/Client/app/Club/club.model';
import { EditClubInformationModal } from './editclubinformation.component';

@Component({
    selector: 'app-clubsmanagment',
    templateUrl: './clubmanagment.component.html',
})
export class ClubManagmentComponent implements OnInit {
    
    ngOnInit(): void {
        this.loadClubDescription();
    }

    constructor(private clubService: ClubService, public dialog: MatDialog) { }

    private clubModel: ClubViewModel;

    async loadClubDescription() {
        await this.clubService.getClubDescription().subscribe(clubModel => this.clubModel = clubModel)       
    }

    openEditClubDialog() {
        this.dialog.open(EditClubInformationModal, {
            data: this.clubModel
        }); { }
    }
}