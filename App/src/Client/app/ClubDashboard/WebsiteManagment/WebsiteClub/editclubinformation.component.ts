import { Inject, Component } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialog } from '@angular/material';
import { NgForm } from '@angular/forms';
import { ErrorsViewModel } from 'src/Client/app/Helpers/Validators/error.model';
import { ClubViewModel } from 'src/Client/app/Club/club.model';
import { ClubService } from 'src/Client/app/Club/club.service';

@Component({
    selector: 'app-editclubinformationdialog',
    templateUrl: 'editclubinformation.component.html'
})

export class EditClubInformationModal {
    submitted = false;
    errors: ErrorsViewModel = new ErrorsViewModel();
    constructor(@Inject(MAT_DIALOG_DATA) public data: ClubViewModel, private clubService: ClubService,
        public dialog: MatDialog) { }

    async updateClubInformation(ngForm: NgForm) {
        this.submitted = true;
        if (ngForm.valid) {
            await this.clubService.putToUpdateClubInformation(this.data)
                .toPromise().then(res => { this.errors = res as ErrorsViewModel })

            if (this.errors.Succeeded == true) {
                this.dialog.closeAll();
                window.location.reload();
            }
            this.submitted = false;
        }
    }
}