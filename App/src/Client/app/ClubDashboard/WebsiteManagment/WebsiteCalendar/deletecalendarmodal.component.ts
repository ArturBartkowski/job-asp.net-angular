import { Inject, Component } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialog } from '@angular/material';
import { ErrorsViewModel } from 'src/Client/app/Helpers/Validators/error.model';
import { EventInput } from '@fullcalendar/core';
import { CalendarService } from 'src/Client/app/Calendar/calendar.service';

@Component({
    templateUrl: 'deletecalendarmodal.component.html'
})

export class DeleteCalendarModal {
    submitted = false;
    errors: ErrorsViewModel = new ErrorsViewModel();
    constructor(@Inject(MAT_DIALOG_DATA) public data: EventInput, private calendarService: CalendarService,
        public dialog: MatDialog) { }

    async deleteCalendarEvent(calendarEventId: number) {
        this.submitted = true;
        await this.calendarService.deleteCalendarEvent(calendarEventId)
            .toPromise().then(res => { this.errors = res as ErrorsViewModel })

        if (this.errors.Succeeded == true) {
            this.dialog.closeAll();
            window.location.reload();
        }
        this.submitted = false;
    }
}