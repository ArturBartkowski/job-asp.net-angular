import { Inject, Component } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialog } from '@angular/material';
import { NgForm } from '@angular/forms';
import { ErrorsViewModel } from 'src/Client/app/Helpers/Validators/error.model';
import { EventInput } from './../../../Calendar/calendar.model';
import { CalendarService } from 'src/Client/app/Calendar/calendar.service';

@Component({
    selector: 'app-editcalendardialog',
    templateUrl: 'editcalendarmodal.component.html'
})

export class EditCalendarModal {
    submitted = false;
    errors: ErrorsViewModel = new ErrorsViewModel();
    constructor(@Inject(MAT_DIALOG_DATA) public data: EventInput, private calendarService: CalendarService, 
    public dialog: MatDialog) { }

    async updateCalendarEvent(ngForm: NgForm) {
        this.submitted = true;
        if (ngForm.valid) {
            await this.calendarService.putToUpdateCalendarEvent(this.data)
                .toPromise().then(res => { this.errors = res as ErrorsViewModel })

            if (this.errors.Succeeded == true) {
                this.dialog.closeAll();
                window.location.reload();
            }
            this.submitted = false;
        }
    }
}