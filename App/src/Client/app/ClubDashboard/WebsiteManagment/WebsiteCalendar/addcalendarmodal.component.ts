import { Inject, Component, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialog } from '@angular/material';
import { NgForm } from '@angular/forms';
import { ErrorsViewModel } from 'src/Client/app/Helpers/Validators/error.model';
import { CalendarService } from 'src/Client/app/Calendar/calendar.service';
import { EventInput } from 'src/Client/app/Calendar/calendar.model';

@Component({
    templateUrl: 'addcalendarmodal.component.html'
})

export class AddCalendarModal implements OnInit {
    submitted = false;
    errors: ErrorsViewModel = new ErrorsViewModel();
    constructor(@Inject(MAT_DIALOG_DATA) public data: EventInput, private calendarService: CalendarService,
        public dialog: MatDialog) { }

    ngOnInit(): void {
        this.data.backgroundColor = "#0000FF"
        this.data.borderColor = "#0000FF"
        this.data.textColor = "#000000"
    }

    async newCalendarEvent(ngForm: NgForm) {
        this.submitted = true;
        if (ngForm.valid) {
            await this.calendarService.requestToAddCalendarEvent(this.data)
                .toPromise().then(res => { this.errors = res as ErrorsViewModel })

            if (this.errors.Succeeded == true) {
                this.dialog.closeAll();
                window.location.reload();
            }
            this.submitted = false;
        }
    }
}