import { EventInput as EventInputHelper } from './../../../Calendar/calendar.model';
import { Component } from '@angular/core';
import { MatDialog } from '@angular/material';
import { CalendarService } from 'src/Client/app/Calendar/calendar.service';
import { AddCalendarModal } from './addcalendarmodal.component';
import { DeleteCalendarModal } from './deletecalendarmodal.component';
import { EditCalendarModal } from './editcalendarmodal.component';

@Component({
  selector: 'app-calendarmanagment',
  templateUrl: './calendarmanagment.component.html',
})
export class CalendarManagmentComponent {
  constructor(private calendarService: CalendarService, public dialog: MatDialog) { }

  private calendarModel: EventInputHelper[] = [];
  private loaded: boolean = false;

  async loadEventInput() {
    this.loaded = true;
    await this.calendarService.getCalendarEventsAlias().subscribe(calendarModel => this.calendarModel = calendarModel)
  }

  getCalendar(): EventInputHelper[] {
    if (!this.loaded) {
      this.loadEventInput();
    }
    return this.calendarModel;
  }

  openAddCalendarDialog() {
    this.dialog.open(AddCalendarModal, {
      data: new EventInputHelper
    }); { }
  }

  openEditCalendarDialog(calendarId: number) {
    this.dialog.open(EditCalendarModal, {
      data: this.calendarModel.find(x => x.id == calendarId)
    }); { }
  }

  openDeleteCalendarDialog(calendarId: number) {
    this.dialog.open(DeleteCalendarModal, {
      data: this.calendarModel.find(x => x.id == calendarId)
    }); { }
  }
}