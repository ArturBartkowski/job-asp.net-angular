import { ToastrService } from 'ngx-toastr';
import { Component } from '@angular/core';
import { GalleryService } from 'src/Client/app/Gallery/gallery.service';
import { ErrorsViewModel } from 'src/Client/app/Helpers/Validators/error.model';

@Component({
    templateUrl: './gallerymanagment.component.html',
})
export class GalleryManagmentComponent {
    errors: ErrorsViewModel = new ErrorsViewModel();
    constructor(private galleryService: GalleryService, private toastrService: ToastrService) { }

    async authGallery() {
        await this.galleryService.requestToAuthGallery()
            .toPromise().then(res => { this.errors = res as ErrorsViewModel })

        if (this.errors.Succeeded == true) {
            this.toastrService.success("Użytkownik jest zautoryzowany.");
        } else {
            window.location.href = this.errors.Errors[0]
        }
    }

    async updateGallery() {
        await this.galleryService.requestToUpdateGallery()
            .toPromise().then(res => { this.errors = res as ErrorsViewModel })

        if (this.errors.Succeeded == true) {
            this.toastrService.success("Galeria została zaktualizowana.");
        } else {
            this.toastrService.warning("Coś poszło nie tak. Spróbuj za chwilę.");
        }
    }
}