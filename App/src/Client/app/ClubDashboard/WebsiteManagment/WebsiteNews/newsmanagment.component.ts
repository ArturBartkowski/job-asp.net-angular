import { NewsService } from 'src/Client/app/News/news.service';
import { NewsViewModel } from 'src/Client/app/News/news.model';
import { Component } from '@angular/core';
import { MatDialog } from '@angular/material';
import { AddNewsModal } from './addnewsmodal.component';
import { EditNewsModal } from './editnewsmodal.component';
import { DeleteNewsModal } from './deletenewsmodal.component';

@Component({
  selector: 'app-newsmanagment',
  templateUrl: './newsmanagment.component.html',
})
export class NewsManagmentComponent {
  constructor(private newsService: NewsService, public dialog: MatDialog) { }

  private newsModel: NewsViewModel[] = [];
  private loaded: boolean = false;

  async loadNews() {
    this.loaded = true;
    await this.newsService.getNews().subscribe(newsModel => this.newsModel = newsModel)
  }

  getNews(): NewsViewModel[] {
    if (!this.loaded) {
      this.loadNews();
    }
    return this.newsModel;
  }

  openAddNewsDialog() {
    this.dialog.open(AddNewsModal, {
      data: new NewsViewModel
    }); { }
  }

  openEditNewsDialog(newId: number) {
    this.dialog.open(EditNewsModal, {
      data: this.newsModel.find(x => x.Id == newId)
    }); { }
  }

  openDeleteNewsDialog(newId: number) {
    this.dialog.open(DeleteNewsModal, {
      data: this.newsModel.find(x => x.Id == newId)
    }); { }
  }
}