import { NewsService } from 'src/Client/app/News/news.service';
import { Inject, Component } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialog } from '@angular/material';
import { NewsViewModel } from 'src/Client/app/News/news.model';
import { ErrorsViewModel } from 'src/Client/app/Helpers/Validators/error.model';

@Component({
    templateUrl: 'deletenewsmodal.component.html'
})

export class DeleteNewsModal {
    submitted = false;
    errors: ErrorsViewModel = new ErrorsViewModel();
    constructor(@Inject(MAT_DIALOG_DATA) public data: NewsViewModel, private newsService: NewsService,
        public dialog: MatDialog) { }

    async deleteNews(newsId: number) {
        this.submitted = true;
        await this.newsService.deleteNews(newsId)
            .toPromise().then(res => { this.errors = res as ErrorsViewModel })

        if (this.errors.Succeeded == true) {
            this.dialog.closeAll();
            window.location.reload();
        }
        this.submitted = false;
    }
}