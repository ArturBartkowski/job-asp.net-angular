import { Inject, Component, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialog } from '@angular/material';
import { NewsViewModel } from 'src/Client/app/News/news.model';
import { NgForm } from '@angular/forms';
import { ErrorsViewModel } from 'src/Client/app/Helpers/Validators/error.model';
import { NewsService } from 'src/Client/app/News/news.service';

@Component({
    selector: 'app-editnewsdialog',
    templateUrl: 'editnewsmodal.component.html'
})

export class EditNewsModal {
    submitted = false;
    errors: ErrorsViewModel = new ErrorsViewModel();
    constructor(@Inject(MAT_DIALOG_DATA) public data: NewsViewModel, private newsService: NewsService, 
    public dialog: MatDialog) { }

    async updateNews(ngForm: NgForm) {
        this.submitted = true;
        if (ngForm.valid) {
            await this.newsService.putToUpdateNews(this.data)
                .toPromise().then(res => { this.errors = res as ErrorsViewModel })

            if (this.errors.Succeeded == true) {
                this.dialog.closeAll();
                window.location.reload();
            }
            this.submitted = false;
        }
    }
}