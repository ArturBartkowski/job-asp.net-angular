import { EditNewsModal } from './WebsiteManagment/WebsiteNews/editnewsmodal.component';
import { DashboardController } from './clubdashboard.controller';
import { ClubDashboardComponent } from './clubdashboard.component';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppGuard } from '../Main/Auth/EastRidersFirstGuard';
import { DynamicScriptLoaderService } from '../Helpers/dynamicscriptloader.service';
import { DashboardService } from './clubdashboard.service';
import { NewsManagmentComponent } from './WebsiteManagment/WebsiteNews/newsmanagment.component';
import { AddNewsModal } from './WebsiteManagment/WebsiteNews/addnewsmodal.component';
import { MatFormFieldModule, MatInputModule, MatDatepickerModule, MatOptionModule, MatSelectModule, MatNativeDateModule, MatDialogModule } from '@angular/material';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DeleteNewsModal } from './WebsiteManagment/WebsiteNews/deletenewsmodal.component';
import { AppRoutingModule } from '../Main/app.routing.module';
import { ClubManagmentComponent } from './WebsiteManagment/WebsiteClub/clubmanagment.component';
import { EditClubInformationModal } from './WebsiteManagment/WebsiteClub/editclubinformation.component';
import { CalendarManagmentComponent } from './WebsiteManagment/WebsiteCalendar/calendarmanagment.component';
import { AddCalendarModal } from './WebsiteManagment/WebsiteCalendar/addcalendarmodal.component';
import { EditCalendarModal } from './WebsiteManagment/WebsiteCalendar/editcalendarmodal.component';
import { DeleteCalendarModal } from './WebsiteManagment/WebsiteCalendar/deletecalendarmodal.component';
import { GalleryManagmentComponent } from './WebsiteManagment/WebsiteGallery/gallerymanagment.component';

@NgModule({
    imports: [BrowserModule, FormsModule, ReactiveFormsModule, MatFormFieldModule, MatInputModule, MatDatepickerModule, MatOptionModule, MatSelectModule,
        MatNativeDateModule, MatDialogModule, AppRoutingModule],
    providers: [AppGuard, DynamicScriptLoaderService, DashboardService, DashboardController, MatDatepickerModule],
    declarations: [ClubDashboardComponent, NewsManagmentComponent, AddNewsModal, EditNewsModal, DeleteNewsModal, ClubManagmentComponent,
        EditClubInformationModal, CalendarManagmentComponent, AddCalendarModal, EditCalendarModal, DeleteCalendarModal, GalleryManagmentComponent],
    exports: [ClubDashboardComponent, NewsManagmentComponent],
    entryComponents: [AddNewsModal, EditNewsModal, DeleteNewsModal, EditClubInformationModal, AddCalendarModal,
        EditCalendarModal, DeleteCalendarModal],
})

export class ClubDashboardModule { }