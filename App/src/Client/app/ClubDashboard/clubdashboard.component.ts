import { Router } from '@angular/router';
import { AccountService } from './../LoginPanel/Account/account.service';
import { DashboardService } from './clubdashboard.service';
import { ClubBasicInformationViewModel } from './clubdashboardbasic.model';
import { Component, ViewEncapsulation } from '@angular/core';
import { DynamicScriptLoaderService } from '../Helpers/dynamicscriptloader.service';
import { HTMLHelpers } from '../Helpers/HTML.helpers';

@Component({
  selector: 'app-clubdashboard',
  templateUrl: './clubdashboard.component.html',
  styleUrls: ['./clubdashboard.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class ClubDashboardComponent {
  constructor(private dynamicScriptLoader: DynamicScriptLoaderService, private htmlHelper: HTMLHelpers,
    private dashboardService: DashboardService, private accountService: AccountService, private router: Router) { }

  private basicModel: ClubBasicInformationViewModel[] = [];
  private loaded: boolean = false;

  ngOnInit(): void {
    this.reloader();
    this.addElementToBody();
    this.loadScripts();
    this.getBasicInformation();
  }

  private async loadScripts() {
    await this.dynamicScriptLoader.load('webfonterc', 'addwebfonterc', 'ktaoptions', 'jquery', 'popper', 'bootstrap4.3.1', 'owl-carousel', 'js-cookie', 'moment', 'tooltip'
      , 'perfect-scrollbar', 'sticky-js', 'wnumb', 'scripts-bundle', 'fontawesome').then(data => {
      }).catch(error => console.log(error));
  }

  private addElementToBody() {
    this.htmlHelper.addClassToBody("kt-quick-panel--right");
    this.htmlHelper.addClassToBody("kt-demo-panel--right");
    this.htmlHelper.addClassToBody("kt-offcanvas-panel--right");
    this.htmlHelper.addClassToBody("kt-header--fixed");
    this.htmlHelper.addClassToBody("kt-header-mobile--fixed");
    this.htmlHelper.addClassToBody("kt-subheader--fixed");
    this.htmlHelper.addClassToBody("kt-subheader--enabled");
    this.htmlHelper.addClassToBody("kt-subheader--solid");
    this.htmlHelper.addClassToBody("kt-aside--enabled");
    this.htmlHelper.addClassToBody("kt-aside--fixed");
  }

  private reloader(): void {
    if (window.localStorage) {
      if (!localStorage.getItem('firstLoad')) {
        localStorage['firstLoad'] = true;
        location.reload(true);
      }
      else
        localStorage.removeItem('firstLoad');
    }
  }

  async loadBasicData() {
    this.loaded = true;
    await this.dashboardService.getBasicUserInformation().subscribe(basicModel => this.basicModel = basicModel)
  }

  getBasicInformation(): ClubBasicInformationViewModel[] {
    if (!this.loaded) {
      this.loadBasicData();
    }
    return this.basicModel;
  }

  logOff() {
    this.accountService.requestToLogOff().toPromise();
    this.router.navigateByUrl("/Home");
  }
}