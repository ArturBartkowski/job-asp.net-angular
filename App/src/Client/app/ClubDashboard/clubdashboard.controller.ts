import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ClubBasicInformationViewModel } from './clubdashboardbasic.model';

@Injectable()
export class DashboardController {

    constructor(private httpClient: HttpClient) { }

    public getBasicInformation(): Observable<ClubBasicInformationViewModel[]> {
        return this.httpClient.get<ClubBasicInformationViewModel[]>(`/Club/GetBasicClubInformation`);
    }
}
