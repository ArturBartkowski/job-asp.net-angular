export class NewsViewModel {
    constructor(
        public Id?: number,
        public Url?: string,
        public CreationDate?: Date,
        public CreatedBy?: string,
        public Title?: string,
        public Description?: string,
        public ShortDescription?: string,
        public Type?: string) { }
}
