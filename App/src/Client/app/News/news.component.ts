import { Component } from '@angular/core';
import { NewsService } from './news.service';
import { NewsViewModel } from './news.model';

@Component({
  selector: 'app-news-main',
  templateUrl: './news.component.html'
})
export class NewsComponent {

  constructor(private service: NewsService) { }
  private news: NewsViewModel[] = [];
  private loaded: boolean = false;

  getNewsByLimit(limit: number): NewsViewModel[] {
    if (!this.loaded) {
      this.service.getNewsByLimit(limit).subscribe(news => this.news = news)
    }
    this.loaded = true;

    return this.news;
  }
}
