import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { NewsComponent } from './news.component';
import { NewsService } from '../News/news.service';
import { NewsController } from '../News/news.controller';

@NgModule({
    imports: [BrowserModule, FormsModule],
    providers: [NewsService, NewsController],
    declarations: [NewsComponent],
    exports: [NewsComponent]
})
export class NewsModule { }
