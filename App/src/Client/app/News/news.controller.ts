import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { NewsViewModel } from './news.model';

@Injectable()
export class NewsController {

    constructor(private httpClient: HttpClient) { }

    public getNewsByLimit(limit: number): Observable<NewsViewModel[]> {
        return this.httpClient.get<NewsViewModel[]>(`/News/GetNewsByLimit?limit=${limit}`);
    }

    public getNews(httpOptions: any): Observable<NewsViewModel[]> {
        return this.httpClient.get<NewsViewModel[]>(`/News/GetNews`, { headers: { httpOptions } });
    }

    public addNews(model: NewsViewModel): Observable<NewsViewModel> {
        return this.httpClient.post("News/AddNews", model) as Observable<NewsViewModel>;
    }

    public updateNews(model: NewsViewModel): Observable<NewsViewModel> {
        return this.httpClient.put("News/UpdateNews", model) as Observable<NewsViewModel>;
    }

    public deleteNews(newsId: number): Observable<any> {
        return this.httpClient.delete(`/News/DeleteNews?id=${newsId}`) as Observable<any>;
    }
}
