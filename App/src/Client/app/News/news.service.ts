import { Injectable } from '@angular/core';
import { NewsViewModel } from './news.model';
import { NewsController } from './news.controller';
import { Observable } from 'rxjs';
import { HttpHeaders } from '@angular/common/http';

@Injectable()
export class NewsService {

    constructor(private dataSource: NewsController) { }

    getNewsByLimit(limit: number): Observable<NewsViewModel[]> {
        return this.dataSource.getNewsByLimit(limit);
    }

    getNews(): Observable<NewsViewModel[]> {
        const httpOptions = {
            headers: new HttpHeaders({
                'Authorization': 'Bearer ' + localStorage.getItem('access_token'),
            })
        };
        return this.dataSource.getNews(httpOptions);
    }

    requestToAddNews(model: NewsViewModel): Observable<NewsViewModel> {
        return this.dataSource.addNews(model);
    }

    putToUpdateNews(model: NewsViewModel): Observable<NewsViewModel> {
        return this.dataSource.updateNews(model);
    }

    deleteNews(newsId: number): Observable<any> {
        return this.dataSource.deleteNews(newsId);
    }
}
