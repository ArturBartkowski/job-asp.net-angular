export class ErrorsViewModel {
    constructor(
        public Succeeded?: boolean,
        public Errors?: string[]) { }
}