import { Injectable } from '@angular/core';

interface Scripts {
    name: string;
    src: string;
}

export const ScriptStore: Scripts[] = [
    { name: 'jquery-min', src: '/Scripts/Jquery/jquery.min.js' },
    { name: 'flickity', src: '/Scripts/flickity/dist/flickity.pkgd.min.js' },
    { name: 'khaki', src: '/Scripts/Khaki/khaki-init.js' },
    { name: 'ofi', src: '/Scripts/object-fit-images/ofi.min.js' },
    { name: 'popper-min', src: '/Scripts/Popper/popper.min.js' },
    { name: 'bootstrap', src: '/Scripts/bootstrap/dist/js/bootstrap.min.js' },
    { name: 'photoswipe', src: '/Scripts/photoswipe/dist/photoswipe.min.js' },
    { name: 'photoswipe-ui-default', src: '/Scripts/photoswipe/dist/photoswipe-ui-default.min.js' },
    { name: 'khaki-min', src: '/Scripts/Khaki/khaki.min.js' },
    { name: 'webfonterc', src: '/Scripts/WebFont/webfonterc.js' },
    { name: 'addwebfonterc', src: '/Scripts/WebFont/addwebfonterc.js' },
    { name: 'jquery', src: '/Scripts/Jquery/jquery.js' },
    { name: 'popper', src: '/Scripts/popper.js/dist/umd/popper.js' },
    { name: 'bootstrap4.3.1', src: '/Scripts/bootstrap/dist/4.3.1/bootstrap.min.js' },
    { name: 'js-cookie', src: '/Scripts/js-cookie/src/js.cookie.js' },
    { name: 'moment', src: '/Scripts/moment/min/moment.min.js' },
    { name: 'tooltip', src: '/Scripts/tooltip.js/dist/umd/tooltip.min.js' },
    { name: 'perfect-scrollbar', src: '/Scripts/perfect-scrollbar/dist/perfect-scrollbar.js' },
    { name: 'sticky-js', src: '/Scripts/sticky-js/dist/sticky.min.js' },
    { name: 'wnumb', src: '/Scripts/wnumb/wNumb.js' },
    { name: 'scripts-bundle', src: '/Scripts/Bundle/scripts.bundle.js' },
    { name: 'login-general', src: '/Scripts/Bundle/login-general.js' },
    { name: 'ktaoptions', src: '/Scripts/WebFont/KTAOptions.js' },
    { name: 'recaptcha', src: 'https://www.google.com/recaptcha/api.js?render=explicit' },
    { name: 'fontawesome', src: '/Scripts/WebFont/allFontawasome.js' },
    { name: 'owl-carousel', src: '/Scripts/owljs/owl.carousel.js' },
    { name: 'embedphotos', src: '/Scripts/EmbedPhotos/Embedphotos.js' }
];

declare var document: any;

@Injectable()
export class DynamicScriptLoaderService {

    private scripts: any = {};

    constructor() {
        ScriptStore.forEach((script: any) => {
            this.scripts[script.name] = {
                loaded: false,
                src: script.src
            };
        });
    }

    load(...scripts: string[]) {
        const promises: any[] = [];
        scripts.forEach((script) => promises.push(this.loadScript(script)));
        return Promise.all(promises);
    }

    loadScript(name: string) {
        return new Promise((resolve, reject) => {
            if (!this.scripts[name].loaded) {
                //load script
                let script = document.createElement('script');
                script.type = 'text/javascript';
                script.src = this.scripts[name].src;
                script.async = true;
                script.defer = true;
                if (script.readyState) {  //IE
                    script.onreadystatechange = () => {
                        if (script.readyState === "loaded" || script.readyState === "complete") {
                            script.onreadystatechange = null;
                            this.scripts[name].loaded = true;
                            resolve({ script: name, loaded: true, status: 'Loaded' });
                        }
                    };
                } else {  //Others
                    script.onload = () => {
                        this.scripts[name].loaded = true;
                        resolve({ script: name, loaded: true, status: 'Loaded' });
                    };
                }
                script.onerror = (error: any) => resolve({ script: name, loaded: false, status: 'Loaded' });
                document.getElementsByTagName('body')[0].appendChild(script);
            } else {
                resolve({ script: name, loaded: true, status: 'Already Loaded' });
            }
        });
    }

    removeScripts(): void {
        var scripts = document.getElementsByTagName('script');
        var i = scripts.length;
        while (i--) {
            scripts[i].parentNode.removeChild(scripts[i]);
        }

        var scripts = document.getElementsByTagName('style');
        var i = scripts.length;
        while (i--) {
            scripts[i].parentNode.removeChild(scripts[i]);
        }
    }

    loadExternalStyles(styleUrl: string) {
        return new Promise((resolve, reject) => {
            const styleElement = document.createElement('link');
            styleElement.href = styleUrl;
            styleElement.onload = resolve;
            document.head.appendChild(styleElement);
        });
    }
}
