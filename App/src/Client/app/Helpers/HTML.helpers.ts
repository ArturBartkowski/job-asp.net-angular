export class HTMLHelpers {

    addClassToBody(className: string) {
        let body = document.getElementsByTagName('body')[0];
        body.classList.add(className);
    }
}