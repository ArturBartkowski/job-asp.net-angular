import { Injectable } from '@angular/core';
import { CalendarController } from './calendar.controller';
import { Observable } from 'rxjs';
import { EventInput as EventInputHelper } from './calendar.model';
import { EventInput } from '@fullcalendar/core';

@Injectable()
export class CalendarService {

    constructor(private dataSource: CalendarController) { }

    getCalendarEvents(): Observable<EventInput[]> {
        return this.dataSource.getCalendarEvents();
    }

    getCalendarEventsAlias(): Observable<EventInputHelper[]> {
        return this.dataSource.getCalendarEventsAlias();
    }

    requestToAddCalendarEvent(model: EventInputHelper): Observable<EventInputHelper> {
        return this.dataSource.addCalendarEvent(model);
    }

    putToUpdateCalendarEvent(model: EventInputHelper): Observable<EventInputHelper> {
        return this.dataSource.updateCalendarEvent(model);
    }

    deleteCalendarEvent(eventId: number): Observable<any> {
        return this.dataSource.deleteCalendarEvent(eventId);
    }
}