import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { CalendarComponent } from './calendar.component';
import { CalendarService } from './calendar.service';
import { CalendarController } from './calendar.controller';
import { FullCalendarComponent } from './calendar.main.component';

@NgModule({
    imports: [BrowserModule, FormsModule],
    providers: [CalendarService, CalendarController],
    declarations: [CalendarComponent, FullCalendarComponent],
    exports: [CalendarComponent]
})
export class FullCalendarModule { }