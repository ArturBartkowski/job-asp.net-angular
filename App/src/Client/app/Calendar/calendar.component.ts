import { Component } from '@angular/core';
import dayGridPlugin from '@fullcalendar/daygrid';
import { CalendarService } from './calendar.service';
import { EventInput } from '@fullcalendar/core';

@Component({
  selector: 'app-calendar-main',
  templateUrl: './calendar.component.html'
})

export class CalendarComponent {
  constructor(private service: CalendarService) { 
    this.loadCalendarData();
  }

  public calendarEvents: EventInput[] = [];
  calendarPlugins = [dayGridPlugin];
  loadCalendarData() {
    this.service.getCalendarEvents().subscribe(calendar => this.calendarEvents = calendar);
  }
}