import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { EventInput as EventInputHelper } from './calendar.model';
import { EventInput } from '@fullcalendar/core';

@Injectable()
export class CalendarController {

    constructor(private httpClient: HttpClient) { }

    public getCalendarEvents(): Observable<EventInput[]> {
        return this.httpClient.get<EventInput[]>(`/Calendar/GetCalendarEvents`);
    }

    public getCalendarEventsAlias(): Observable<EventInputHelper[]> {
        return this.httpClient.get<EventInputHelper[]>(`/Calendar/GetCalendarEvents`);
    }

    public addCalendarEvent(model: EventInputHelper): Observable<EventInputHelper> {
        return this.httpClient.post("Calendar/AddCalendarEvent", model) as Observable<EventInputHelper>;
    }

    public updateCalendarEvent(model: EventInputHelper): Observable<EventInputHelper> {
        return this.httpClient.put("Calendar/UpdateCalendarEvent", model) as Observable<EventInputHelper>;
    }

    public deleteCalendarEvent(eventId: number): Observable<any> {
        return this.httpClient.delete(`/Calendar/DeleteCalendarEvent?id=${eventId}`) as Observable<any>;
    }
}