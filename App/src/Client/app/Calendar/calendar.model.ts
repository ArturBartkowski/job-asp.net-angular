export class EventInput {
    constructor(
        public id?: number,
        public title?: string,
        public allDay?: boolean,
        public backgroundColor?: string,
        public borderColor?: string,
        public start?: Date,
        public end?: Date,
        public textColor?: string,
        public url?: string) { }
}
