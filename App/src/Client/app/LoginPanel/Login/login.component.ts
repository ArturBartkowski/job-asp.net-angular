import { Router } from '@angular/router';
import { TokenViewModel } from './token.model';
import { Component } from '@angular/core';
import { AccountService } from '../Account/account.service';
import { NgForm } from '@angular/forms';
import { LoginViewModel } from './login.model';
import { ErrorsViewModel } from '../../Helpers/Validators/error.model';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html'
})
export class LoginComponent {
    loginModel: LoginViewModel = new LoginViewModel();
    submitted = false;
    private token: TokenViewModel = new TokenViewModel();
    errors: ErrorsViewModel = new ErrorsViewModel();
    constructor(private accountService: AccountService, private router: Router) { }

    async login(form: NgForm) {
        this.submitted = true;
        if (form.valid) {
            await this.accountService.requestToLogin(this.loginModel)
                .toPromise().then(res => { this.token = res as TokenViewModel })

            if (this.token.Succeeded == false) {
                this.errors.Errors = this.token.Errors;
            } else {
                localStorage.setItem("access_token", this.token.Access_token);
                this.router.navigateByUrl('/home');
            }
            this.submitted = false;
        }
    }
}