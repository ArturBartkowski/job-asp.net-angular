export class LoginViewModel {
    constructor(
        public UserName?: string,
        public Password?: string,
        public ReturnUrl?: string,
        public RememberMe?: boolean) { }
}