export class TokenViewModel {
    constructor(
        public Access_token?: string,
        public Refresh_token?: string,
        public Succeeded?: boolean,
        public Errors?: string[]) { }
}