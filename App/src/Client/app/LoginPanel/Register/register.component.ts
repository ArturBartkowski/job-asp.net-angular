import { ErrorsViewModel } from '../../Helpers/Validators/error.model';
import { Component } from '@angular/core';
import { NgForm } from '@angular/forms';
import { RegisterViewModel } from './register.model';
import { AccountService } from '../Account/account.service';
import { Router } from '@angular/router';
import { Cookies } from '../../Helpers/cookie.service';

@Component({
    selector: 'app-register',
    templateUrl: './register.component.html'
})
export class RegisterComponent {
    registerModel: RegisterViewModel = new RegisterViewModel();
    submitted = false;
    errors: ErrorsViewModel = new ErrorsViewModel();
    constructor(private accountService: AccountService, private router: Router, private cookies: Cookies) { }

    async register(form: NgForm) {
        this.submitted = true;
        if (form.valid) {
            await this.accountService.requestToRegister(this.registerModel)
                .toPromise().then(res => { this.errors = res as ErrorsViewModel })
            this.submitted = false;

            if (this.errors.Succeeded === true) {
                this.cookies.setCookie("confirmEmail", "true");
                this.router.navigateByUrl("/Home");
            } else { 
                this.registerModel.ReCaptcha = '';
            }
        }
    }
}