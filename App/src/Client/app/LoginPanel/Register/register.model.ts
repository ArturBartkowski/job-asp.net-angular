export class RegisterViewModel {
    constructor(
        public Name?: string,
        public Email?: string,
        public Password?: string,
        public ConfirmPassword?: string,
        public Agree?: boolean,
        public ReCaptcha?: string) { }
}