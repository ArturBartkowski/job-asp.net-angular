import { AccountController } from './Account/account.controller';
import { AccountService } from './Account/account.service';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { LoginPanelComponent } from './loginpanel.component';
import { RegisterComponent } from './Register/register.component';
import { LoginComponent } from './Login/login.component';
import { RouterModule } from "@angular/router";
import { HTMLHelpers } from '../Helpers/HTML.helpers';
import { RecaptchaModule } from 'ng-recaptcha';
import { RecaptchaFormsModule } from 'ng-recaptcha/forms';
import { MustMatchDirective } from '../Helpers/Validators/mustmatchdirective';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { ForgotPasswordComponent } from './ForgotPassword/forgotpassword.component';
import { EmailApprovedComponent } from '../Notice/ConfirmEmailNotice/emailapproved.component';
import { EmailUnApprovedComponent } from '../Notice/ConfirmEmailNotice/emailunapproved.component';
import { ResetPasswordComponent } from './ForgotPassword/ResetPassword/resetpassword.component';
import { BadCodeComponent } from '../Notice/BadCode/badcode.component';

@NgModule({
    imports: [BrowserModule, FormsModule, RouterModule, RecaptchaModule, RecaptchaFormsModule, MatProgressSpinnerModule,],
    providers: [HTMLHelpers, AccountService, AccountController],
    declarations: [LoginComponent, RegisterComponent, LoginPanelComponent, MustMatchDirective, ForgotPasswordComponent, 
        EmailApprovedComponent, EmailUnApprovedComponent, BadCodeComponent, ResetPasswordComponent],
    exports: [LoginPanelComponent]
})

export class LoginPanelModule { }