import { Component, ViewEncapsulation } from '@angular/core';
import { DynamicScriptLoaderService } from '../Helpers/dynamicscriptloader.service';
import { HTMLHelpers } from '../Helpers/HTML.helpers';

@Component({
  selector: 'app-loginpanel',
  templateUrl: './loginpanel.component.html',
  styleUrls: ['./loginpanel.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class LoginPanelComponent {
  constructor(private dynamicScriptLoader: DynamicScriptLoaderService, private htmlHelper: HTMLHelpers) { }
  ngOnInit(): void {
    this.reloader();
    this.addElementToBody();
    this.loadScripts();
  }

  private async loadScripts() {
    await this.dynamicScriptLoader.load('ktaoptions', 'jquery', 'popper', 'bootstrap4.3.1', 'js-cookie', 'moment', 'tooltip'
      , 'perfect-scrollbar', 'sticky-js', 'wnumb', 'scripts-bundle', 'login-general', 'recaptcha').then(data => {
      }).catch(error => console.log(error));
  }

  private addElementToBody() {
    this.htmlHelper.addClassToBody("kt-quick-panel--right");
    this.htmlHelper.addClassToBody("kt-demo-panel--right");
    this.htmlHelper.addClassToBody("kt-offcanvas-panel--right");
    this.htmlHelper.addClassToBody("kt-header--fixed");
    this.htmlHelper.addClassToBody("kt-header-mobile--fixed");
    this.htmlHelper.addClassToBody("kt-subheader--fixed");
    this.htmlHelper.addClassToBody("kt-subheader--enabled");
    this.htmlHelper.addClassToBody("kt-subheader--solid");
    this.htmlHelper.addClassToBody("kt-aside--enabled");
    this.htmlHelper.addClassToBody("kt-aside--fixed");
  }

  private reloader(): void {
    if (window.localStorage) {
      if (!localStorage.getItem('firstLoad')) {
        localStorage['firstLoad'] = true;
        location.reload(true);
      }
      else
        localStorage.removeItem('firstLoad');
    }
  }
}