import { HTMLHelpers } from 'src/Client/app/Helpers/HTML.helpers';
import { DynamicScriptLoaderService } from './../../../Helpers/dynamicscriptloader.service';
import { Component, ViewEncapsulation, OnInit } from '@angular/core';
import { ErrorsViewModel } from 'src/Client/app/Helpers/Validators/error.model';
import { AccountService } from '../../Account/account.service';
import { Router } from '@angular/router';
import { Cookies } from 'src/Client/app/Helpers/cookie.service';
import { ResetPasswordViewModel } from './resetpassword.model';
import { NgForm } from '@angular/forms';

@Component({
    templateUrl: './resetpassword.component.html',
    styleUrls: ['../../loginpanel.component.css'],
    encapsulation: ViewEncapsulation.None
})
export class ResetPasswordComponent implements OnInit {
    constructor(private accountService: AccountService, private router: Router, private cookies: Cookies,
        private dynamicScriptLoader: DynamicScriptLoaderService, private htmlHelper: HTMLHelpers) { }

    resetPasswordModel: ResetPasswordViewModel = new ResetPasswordViewModel();
    submitted = false;
    errors: ErrorsViewModel = new ErrorsViewModel();

    ngOnInit(): void {
        this.reloader();
        this.addElementToBody();
        this.loadScripts();
    }

    async resetPassword(form: NgForm) {
        this.submitted = true;
        if (form.valid) {
            this.resetPasswordModel.Code = this.cookies.getCookie("resetCode");
            await this.accountService.requestToResetPassword(this.resetPasswordModel)
                .toPromise().then(res => { this.errors = res as ErrorsViewModel })
            this.submitted = false;

            if (this.errors.Succeeded === true) {
                this.cookies.deleteCookie("resetCode");
                this.cookies.setCookie("resetPassword", "true");
                this.router.navigateByUrl("/Home");
            }
        }
    }

    private loadScripts() {
        this.dynamicScriptLoader.load('ktaoptions', 'jquery', 'popper', 'bootstrap4.3.1', 'js-cookie', 'moment', 'tooltip'
            , 'perfect-scrollbar', 'sticky-js', 'wnumb', 'scripts-bundle', 'login-general').then(data => {
            }).catch(error => console.log(error));
    }

    private addElementToBody() {
        this.htmlHelper.addClassToBody("kt-quick-panel--right");
        this.htmlHelper.addClassToBody("kt-demo-panel--right");
        this.htmlHelper.addClassToBody("kt-offcanvas-panel--right");
        this.htmlHelper.addClassToBody("kt-header--fixed");
        this.htmlHelper.addClassToBody("kt-header-mobile--fixed");
        this.htmlHelper.addClassToBody("kt-subheader--fixed");
        this.htmlHelper.addClassToBody("kt-subheader--enabled");
        this.htmlHelper.addClassToBody("kt-subheader--solid");
        this.htmlHelper.addClassToBody("kt-aside--enabled");
        this.htmlHelper.addClassToBody("kt-aside--fixed");
    }

    private reloader(): void {
        if (window.localStorage) {
            if (!localStorage.getItem('firstLoad')) {
                localStorage['firstLoad'] = true;
                location.reload(true);
            }
            else
                localStorage.removeItem('firstLoad');
        }
    }
}