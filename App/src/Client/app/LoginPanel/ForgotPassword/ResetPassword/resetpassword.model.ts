export class ResetPasswordViewModel {
    constructor(
        public Email?: string,
        public Password?: string,
        public ConfirmPassword?: string,
        public Code?: string) { }
}