import { Router } from '@angular/router';
import { Component } from '@angular/core';
import { AccountService } from '../Account/account.service';
import { NgForm } from '@angular/forms';
import { ForgotPasswordViewModel } from './forgotpassword.model';
import { ErrorsViewModel } from '../../Helpers/Validators/error.model';
import { Cookies } from '../../Helpers/cookie.service';

@Component({
    selector: 'app-forgotpassword',
    templateUrl: './forgotpasssword.component.html'
})
export class ForgotPasswordComponent {
    constructor(private accountService: AccountService, private router: Router, private cookies: Cookies) { }

    forgotPasswordModel: ForgotPasswordViewModel = new ForgotPasswordViewModel();
    submitted = false;
    errors: ErrorsViewModel = new ErrorsViewModel();

    async forgotpassword(form: NgForm) {
        this.submitted = true;
        if (form.valid) {
            await this.accountService.requestToForgotPassword(this.forgotPasswordModel)
                .toPromise().then(res => { this.errors = res as ErrorsViewModel })
            this.submitted = false;

            if (this.errors.Succeeded === true) {
                this.cookies.setCookie("forgotPassword", "true");
                this.router.navigateByUrl("/Home");
            } else {
                this.forgotPasswordModel.ReCaptcha = '';
            }
        }
    }
}