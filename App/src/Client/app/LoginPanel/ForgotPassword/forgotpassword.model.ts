export class ForgotPasswordViewModel {
    constructor(
        public Email?: string,
        public ReCaptcha?: string) { }
}