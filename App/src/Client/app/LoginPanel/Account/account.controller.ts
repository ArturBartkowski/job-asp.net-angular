import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { RegisterViewModel } from '../Register/register.model';
import { Observable } from 'rxjs';
import { ForgotPasswordViewModel } from '../ForgotPassword/forgotpassword.model';
import { LoginViewModel } from '../Login/login.model';
import { ResetPasswordViewModel } from '../ForgotPassword/ResetPassword/resetpassword.model';

@Injectable()
export class AccountController {

  constructor(private httpClient: HttpClient) { }

  public register(registerModel: RegisterViewModel, httpOptions: any): Observable<RegisterViewModel> {
    return this.httpClient.post("Account/Register", registerModel, httpOptions) as Observable<RegisterViewModel>;
  }

  public forgotPassword(forgotPasswordModel: ForgotPasswordViewModel, httpOptions: any): Observable<ForgotPasswordViewModel> {
    return this.httpClient.post("Account/ForgotPassword", forgotPasswordModel, httpOptions) as Observable<ForgotPasswordViewModel>;
  }

  public login(loginModel: LoginViewModel, httpOptions: any): Observable<LoginViewModel> {
    return this.httpClient.post("Account/Login", loginModel, httpOptions) as Observable<LoginViewModel>;
  }

  public resetPassword(resetPasswordModel: ResetPasswordViewModel, httpOptions: any): Observable<ResetPasswordViewModel> {
    return this.httpClient.post("Account/ResetPassword", resetPasswordModel, httpOptions) as Observable<ResetPasswordViewModel>;
  }

  public logOff(httpOptions: any) {
    return this.httpClient.post("Account/LogOff", null, httpOptions);
  }
}