import { Injectable } from '@angular/core';
import { RegisterViewModel } from '../Register/register.model';
import { AccountController } from './account.controller';
import { HttpHeaders } from '@angular/common/http';
import { ForgotPasswordViewModel } from '../ForgotPassword/forgotpassword.model';
import { LoginViewModel } from '../Login/login.model';
import { ResetPasswordViewModel } from '../ForgotPassword/ResetPassword/resetpassword.model';
declare var $: any;

@Injectable()
export class AccountService {
    constructor(private dataSource: AccountController) { }

    public requestToRegister(registerModel: RegisterViewModel) {
        const httpOptions = {
            headers: new HttpHeaders({
                'X-XSRF-Token': $('input[name=__RequestVerificationToken]').val(),
            })
        };

        return this.dataSource.register(registerModel, httpOptions);
    }

    public requestToForgotPassword(forgotpasswordModel: ForgotPasswordViewModel) {
        const httpOptions = {
            headers: new HttpHeaders({
                'X-XSRF-Token': $('input[name=__RequestVerificationToken]').val(),
            })
        };

        return this.dataSource.forgotPassword(forgotpasswordModel, httpOptions);
    }

    public requestToLogin(loginModel: LoginViewModel) {
        const httpOptions = {
            headers: new HttpHeaders({
                'X-XSRF-Token': $('input[name=__RequestVerificationToken]').val(),
            })
        };

        return this.dataSource.login(loginModel, httpOptions);
    }

    public requestToResetPassword(resetPasswordModel: ResetPasswordViewModel) {
        const httpOptions = {
            headers: new HttpHeaders({
                'X-XSRF-Token': $('input[name=__RequestVerificationToken]').val(),
            })
        };

        return this.dataSource.resetPassword(resetPasswordModel, httpOptions);
    }

    public requestToLogOff() {
        const httpOptions = {
            headers: new HttpHeaders({
                'Authorization': 'Bearer ' + localStorage.getItem('access_token'),
            })
        };

        let result = this.dataSource.logOff(httpOptions)
        localStorage.removeItem("access_token");
        return result;
    }

    public roleMatch(allowedRoles: string[]): boolean {
        var payLoad = JSON.parse(window.atob(localStorage.getItem('access_token').split('.')[1]));
        var userRole = payLoad["http://schemas.microsoft.com/ws/2008/06/identity/claims/role"] as string[];

        for (var i = 0; i < allowedRoles.length; i++) {
            if (userRole.includes(allowedRoles[i])) {
                return true;
            }
        }

        return false;
    }

    public oneRoleMatch(allowedRole: string): boolean {
        var payLoad = JSON.parse(window.atob(localStorage.getItem('access_token').split('.')[1]));
        var userRole = payLoad["http://schemas.microsoft.com/ws/2008/06/identity/claims/role"] as string[];

        if (userRole != null) {
            if (userRole.includes(allowedRole)) {
                return true;
            }
        }

        return false;
    }

    public isAuth(): boolean {
        var payLoad = localStorage.getItem('access_token');

        if (payLoad != null) {
            return true;
        }

        return false;
    }
}