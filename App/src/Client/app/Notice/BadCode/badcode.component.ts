import { Component, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';
import { HTMLHelpers } from '../../Helpers/HTML.helpers';

@Component({
    templateUrl: "./badcode.component.html",
    styleUrls: ['../ConfirmEmailNotice/emailnotice.component.css'],
    encapsulation: ViewEncapsulation.None
})

export class BadCodeComponent {
    constructor(private router: Router, private htmlHelper: HTMLHelpers) { }

    ngOnInit(): void {
        this.reloader();
        this.addElementToBody();
        this.notice();
    }

    notice() {
        setTimeout(() => {
            this.router.navigateByUrl("/Login")
        }, 10000);
    }

    private addElementToBody() {
        this.htmlHelper.addClassToBody("kt-quick-panel--right");
        this.htmlHelper.addClassToBody("kt-demo-panel--right");
        this.htmlHelper.addClassToBody("kt-offcanvas-panel--right");
        this.htmlHelper.addClassToBody("kt-header--fixed");
        this.htmlHelper.addClassToBody("kt-header-mobile--fixed");
        this.htmlHelper.addClassToBody("kt-subheader--fixed");
        this.htmlHelper.addClassToBody("kt-subheader--enabled");
        this.htmlHelper.addClassToBody("kt-subheader--solid");
        this.htmlHelper.addClassToBody("kt-aside--enabled");
        this.htmlHelper.addClassToBody("kt-aside--fixed");
    }

    private reloader(): void {
        if (window.localStorage) {
            if (!localStorage.getItem('firstLoad')) {
                localStorage['firstLoad'] = true;
                location.reload(true);
            }
            else
                localStorage.removeItem('firstLoad');
        }
    }
}