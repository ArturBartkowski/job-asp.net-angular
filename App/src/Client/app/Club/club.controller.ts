import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ClubViewModel } from './club.model';

@Injectable()
export class ClubController {

    constructor(private httpClient: HttpClient) { }

    public getClubDescription(): Observable<ClubViewModel> {
        return this.httpClient.get<ClubViewModel>(`/Club/GetClubDescription`);
    }

    public updateClubInformation(model: ClubViewModel): Observable<ClubViewModel> {
        return this.httpClient.put("Club/UpdateClubInformation", model) as Observable<ClubViewModel>;
    }
}