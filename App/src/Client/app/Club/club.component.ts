import { Component } from '@angular/core';
import { ClubViewModel } from './club.model';
import { ClubService } from './club.service';

@Component({
    selector: 'app-club-main',
    templateUrl: './club.component.html'
})

export class ClubComponent {
    constructor(private service: ClubService) { }
    private club: ClubViewModel = new ClubViewModel;
    private loaded: boolean = false;

    loadClubData() {
        this.loaded = true;
        this.service.getClubDescription().subscribe(club => this.club = club)
    }

    getClubDescription(): ClubViewModel {
        if (!this.loaded) {
            this.loadClubData();
        }
        return this.club;
    }
}
