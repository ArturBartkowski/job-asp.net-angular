import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { ClubComponent } from './club.component';
import { ClubService } from './club.service';
import { ClubController } from './club.controller';

@NgModule({
    imports: [BrowserModule, FormsModule],
    providers: [ClubService, ClubController],
    declarations: [ClubComponent],
    exports: [ClubComponent]
})
export class ClubModule { }
