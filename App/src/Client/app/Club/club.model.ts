export class ClubViewModel {
    constructor(
        public Id?: number,
        public Description?: string,
        public President?: string,
        public VPresidentSecretary?: string,
        public VPresident?: string,
        public Treasurer?: string,
        public Photographer?: string,
        public GraphicDesigner?: string,
        public Chairman?: string,
        public Member1?: string,
        public Member2?: string) { }
}