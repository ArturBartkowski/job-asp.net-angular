import { Injectable } from '@angular/core';
import { ClubViewModel } from './club.model';
import { ClubController } from './club.controller';
import { Observable } from 'rxjs';

@Injectable()
export class ClubService {

    constructor(private dataSource: ClubController) { }

    getClubDescription(): Observable<ClubViewModel> {
        return this.dataSource.getClubDescription();
    }

    putToUpdateClubInformation(model: ClubViewModel): Observable<ClubViewModel> {
        return this.dataSource.updateClubInformation(model);
    }
}