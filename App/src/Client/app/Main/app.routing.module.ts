import { BadCodeComponent } from './../Notice/BadCode/badcode.component';
import { ResetPasswordComponent } from './../LoginPanel/ForgotPassword/ResetPassword/resetpassword.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainPageComponent } from './mainpage.component';
import { LoginPanelComponent } from '../LoginPanel/loginpanel.component';
import { ClubDashboardComponent } from '../ClubDashboard/clubdashboard.component';
import { AppGuard } from './Auth/EastRidersFirstGuard';
import { EmailApprovedComponent } from '../Notice/ConfirmEmailNotice/emailapproved.component';
import { EmailUnApprovedComponent } from '../Notice/ConfirmEmailNotice/emailunapproved.component';
import { ForbiddenComponent } from '../Notice/ErrorCodes/403/forbidden.component';
import { NotFoundComponent } from '../Notice/ErrorCodes/404/notfound.component';
import { GeneralErrorComponent } from '../Notice/ErrorCodes/General/generalerror.component';
import { NewsManagmentComponent } from '../ClubDashboard/WebsiteManagment/WebsiteNews/newsmanagment.component';
import { ClubManagmentComponent } from '../ClubDashboard/WebsiteManagment/WebsiteClub/clubmanagment.component';
import { CalendarManagmentComponent } from '../ClubDashboard/WebsiteManagment/WebsiteCalendar/calendarmanagment.component';
import { GalleryManagmentComponent } from '../ClubDashboard/WebsiteManagment/WebsiteGallery/gallerymanagment.component';

const routes: Routes = [
  { path: "Home", component: MainPageComponent },
  { path: "Login", component: LoginPanelComponent },
  { path: "EmailApproved", component: EmailApprovedComponent },
  { path: "EmailUnapproved", component: EmailUnApprovedComponent },
  { path: "ResetPassword", component: ResetPasswordComponent },
  { path: "BadCode", component: BadCodeComponent },
  { path: "Forbidden", component: ForbiddenComponent },
  { path: "NotFound", component: NotFoundComponent },
  { path: "SomethingWrong", component: GeneralErrorComponent },
  {
    path: "ClubDashboard", component: ClubDashboardComponent, canActivate: [AppGuard], data: { permittedRoles: ['Member'] },
    children: [
      { path: "NewsDashboard", component: NewsManagmentComponent, canActivate: [AppGuard], data: { permittedRoles: ['Admin', 'Moderator'] } },
      { path: "ClubInformationDashboard", component: ClubManagmentComponent, canActivate: [AppGuard], data: { permittedRoles: ['Admin', 'Moderator'] } },
      { path: "CalendarDashboard", component: CalendarManagmentComponent, canActivate: [AppGuard], data: { permittedRoles: ['Admin', 'Moderator'] } },
      { path: "GalleryDashboard", component: GalleryManagmentComponent, canActivate: [AppGuard], data: { permittedRoles: ['Admin', 'Moderator'] } },
    ]
  },
  { path: '**', redirectTo: 'Home' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
