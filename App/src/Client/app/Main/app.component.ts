import { Component } from "@angular/core";

@Component({
  selector: "app-east-riders",
  template: "<router-outlet></router-outlet>"
})
export class AppComponent {
  constructor() { }
}


