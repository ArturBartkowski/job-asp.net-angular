import { Cookies } from './../Helpers/cookie.service';
import { AccountService } from '../LoginPanel/Account/account.service';
import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { DynamicScriptLoaderService } from '../Helpers/dynamicscriptloader.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-main',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class MainPageComponent implements OnInit {
  constructor(private dynamicScriptLoader: DynamicScriptLoaderService, private accountService: AccountService,
    private toastrService: ToastrService, private cookie: Cookies) { }
  dateTimeNow = new Date();
  isAuth: boolean = false;
  isMember: boolean = false;

  ngOnInit() {
    this.reloader();
    this.loadScripts();
    if (localStorage.getItem("firstLoad") != "true") {
      this.Notice();
    }
    this.IsAuthenticated();
  }

  private async IsAuthenticated() {
    this.isAuth = await this.accountService.isAuth();
    if (this.isAuth === true) {
      this.isMember = await this.accountService.oneRoleMatch("Member");
    }
  }

  private async loadScripts() {
    await this.dynamicScriptLoader.load('jquery-min', 'ofi', 'popper-min', 'bootstrap', 'flickity', 'embedphotos',
      'khaki-min', 'khaki').then(data => {
      }).catch(error => console.log(error));
  }

  private reloader() {
    if (window.localStorage) {
      if (!localStorage.getItem('firstLoad')) {
        localStorage['firstLoad'] = true;
        location.reload(true);
      }
      else
        localStorage.removeItem('firstLoad');
    }
  }

  private Notice() {
    if (this.cookie.getCookie("confirmEmail") == "true") {
      this.toastrService.success("Rejestracja zakończona.", "Aby aktywować konto, należy potwierdzić e-mail.",
        { closeButton: true, timeOut: 10000 });
      this.cookie.deleteCookie("confirmEmail");
    }
    if (this.cookie.getCookie("forgotPassword") == "true") {
      this.toastrService.success("Sprawdź e-mail.", "Link do zrestartowania hasła został wysłany na e-mail.",
        { closeButton: true, timeOut: 10000 });
      this.cookie.deleteCookie("forgotPassword");
    }
    if (this.cookie.getCookie("resetPassword") == "true") {
      this.toastrService.success("Hasło zrestartowane.", "Możesz się zalogować.",
        { closeButton: true, timeOut: 10000 });
      this.cookie.deleteCookie("resetPassword");
    }
  }
}
