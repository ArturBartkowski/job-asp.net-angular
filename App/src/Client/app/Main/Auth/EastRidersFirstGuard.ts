import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { AccountService } from '../../LoginPanel/Account/account.service';

@Injectable()
export class AppGuard implements CanActivate {

    constructor(private router: Router, private accountService: AccountService) { }

    canActivate(
        next: ActivatedRouteSnapshot,
        state: RouterStateSnapshot): boolean {
        if (localStorage.getItem('access_token') != null) {
            const roles = next.data['permittedRoles'] as Array<string>;
            if (roles) {
                if (this.accountService.roleMatch(roles)) { return true; } else {
                    this.router.navigate(['/Forbidden']);
                    return false;
                }
            }
            return true;
        } else {
            this.router.navigate(['/Login']);
            return false;
        }
    }
}
