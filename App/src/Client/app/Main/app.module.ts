import { ClubDashboardModule } from './../ClubDashboard/clubdashboard.module';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { ClubModule } from '../Club/club.module';
import { NewsModule } from '../News/news.module';
import { FullCalendarModule } from '../Calendar/calendar.module';
import { GalleryModule } from '../Gallery/gallery.module';
import { LoginPanelModule } from '../LoginPanel/loginpanel.module';
import { AppGuard } from './Auth/EastRidersFirstGuard';
import { MainPageComponent } from './mainpage.component';
import { HashLocationStrategy, LocationStrategy } from '@angular/common';
import { DynamicScriptLoaderService } from '../Helpers/dynamicscriptloader.service';
import { AppRoutingModule } from './app.routing.module';
import { AuthInterceptor } from './Auth/AuthInterceptor';
import { LoaderService } from '../Helpers/Spinner/spinner.service';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { LoaderComponent } from '../Helpers/Spinner/spinner.component';
import { LoaderInterceptor } from '../Helpers/Spinner/spinner.interceptor';
import { ToastrService, ToastrModule } from 'ngx-toastr';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { Cookies } from '../Helpers/cookie.service';
import { ForbiddenComponent } from '../Notice/ErrorCodes/403/forbidden.component';
import { NotFoundComponent } from '../Notice/ErrorCodes/404/notfound.component';
import { GeneralErrorComponent } from '../Notice/ErrorCodes/General/generalerror.component';
import { MatDialogModule } from '@angular/material';

@NgModule({
  declarations: [
    AppComponent, MainPageComponent, LoaderComponent, ForbiddenComponent, NotFoundComponent, GeneralErrorComponent
  ],
  imports: [
    BrowserModule, HttpClientModule, ClubModule, NewsModule, FullCalendarModule, GalleryModule, LoginPanelModule,
    AppRoutingModule, ClubDashboardModule, MatProgressSpinnerModule, BrowserAnimationsModule, ToastrModule.forRoot(),
    MatDialogModule
  ],
  providers: [AppGuard, { provide: LocationStrategy, useClass: HashLocationStrategy }, DynamicScriptLoaderService, LoaderService,
    { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true }, { provide: HTTP_INTERCEPTORS, useClass: LoaderInterceptor, multi: true },
    ToastrService, Cookies],
  bootstrap: [AppComponent, LoaderComponent],
})
export class AppModule { }
