﻿using Google.Apis.Auth.OAuth2.Mvc;

namespace EastRidersWithAngular.Controllers
{
    public class AuthCallbackController : Google.Apis.Auth.OAuth2.Mvc.Controllers.AuthCallbackController
    {
        protected override FlowMetadata FlowData => new AppFlowMetadata();
    }
}