﻿using EastRidersWithAngular.Services.GalleryService;
using EastRidersWithAngular.ViewModels.ErrorsViewModel;
using Google.Apis.Auth.OAuth2.Mvc;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace EastRidersWithAngular.Controllers
{
    [Authorize]
    public class GalleryController : Controller
    {
        private readonly IGalleryService _galleryService;
        private CancellationToken _cancellationToken;

        public GalleryController(IGalleryService galleryService)
        {
            _galleryService = galleryService;
        }

        [HttpGet]
        [AllowAnonymous]
        public ActionResult GetGalleryByLimit(int limit)
        {
            var model = _galleryService.GetGalleriesByLimit(limit);

            return Json(model, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<ActionResult> IndexAsync(CancellationToken cancellationToken)
        {
            var result = await new AuthorizationCodeMvcApp(this, new AppFlowMetadata()).AuthorizeAsync(cancellationToken);

            if (result.Credential != null)
            {
                _cancellationToken = cancellationToken;
                return new RedirectResult("/#/ClubDashboard/GalleryDashboard");
            }
            else
            {
                return Json(new ErrorViewModel { Succeeded = false, Errors = new string[] { result.RedirectUri } }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        [Authorize(Roles = "Administrator,Graphic,Fotograf,Moderator")]
        public async Task<ActionResult> UpdateGallery()
        {
            var result = await new AuthorizationCodeMvcApp(this, new AppFlowMetadata()).AuthorizeAsync(_cancellationToken);

            if (result.Credential != null)
            {
                var albums = _galleryService.GetAlbumsFromGooglePhotos(result.Credential.Token.AccessToken);
                var gallery = _galleryService.GetImagesToAlbumsFromGooglePhotos(result.Credential.Token.AccessToken, albums);
                var dbresult = _galleryService.SaveAlbums(gallery);

                return Json(new ErrorViewModel { Succeeded = true });
            }
            else
            {
                return Json(new ErrorViewModel { Succeeded = false });
            }
        }
    }
}
