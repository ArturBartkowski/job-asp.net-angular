﻿using System;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using EastRiders.BL.DatabaseLogic;
using EastRidersWithAngular.Services.AccountService;
using EastRidersWithAngular.ViewModels.Models;
using EastRidersWithAngular.WebAttributes;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.OAuth;
using Newtonsoft.Json.Linq;

namespace EastRidersWithAngular.Controllers
{
    [Authorize]
    public class AccountController : Controller
    {
        private readonly IAccountService _accountService;

        public AccountController(IAccountService accountService)
        {
            _accountService = accountService;
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryTokenERC]
        public async Task<ActionResult> Login(LoginViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return Json(IdentityResult.Failed(ModelState.Values.SelectMany(m => m.Errors).Select(e => e.ErrorMessage).ToArray()));
            }

            var loginResult = await _accountService.GetJWTToken(model);

            if (loginResult.Error_description != null)
            {
                return Json(IdentityResult.Failed(loginResult.Error_description));
            }

            return Json(loginResult);
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryTokenERC]
        public async Task<ActionResult> Register(RegisterViewModel model)
        {
            if (ReCAPTCHA(model.ReCaptcha) == true)
            {
                if (ModelState.IsValid)
                {
                    var user = new ApplicationUser { UserName = model.Name, Email = model.Email };
                    var result = await _accountService.CreateAsyncAccount(model, user);
                    if (result.Succeeded)
                    {
                        await _accountService.SignInAsync(user);

                        string code = await _accountService.GenerateEmailConfirmationTokenAsync(user.Id);
                        var callbackUrl = Url.Action("ConfirmEmail", "Account", new { userId = user.Id, code }, protocol: Request.Url.Scheme);
                        await _accountService.SendConfirmEmailAsync(user.Id, callbackUrl, user, this);

                        return Json(IdentityResult.Success);
                    }
                    AddErrors(result);
                }
            }
            else
            {
                var ReCAPTCHAFailResult = new IdentityResult("Należy zaznaczyć 'Nie jestem robotem'");
                AddErrors(ReCAPTCHAFailResult);

                return Json(IdentityResult.Failed(ModelState.Values.SelectMany(m => m.Errors).Select(e => e.ErrorMessage).ToArray()));
            }

            return Json(IdentityResult.Failed(ModelState.Values.SelectMany(m => m.Errors).Select(e => e.ErrorMessage).ToArray()));
        }

        [AllowAnonymous]
        public async Task<ActionResult> ConfirmEmail(string userId, string code)
        {
            if (userId == null || code == null)
            {
                return new RedirectResult("/#/EmailUnapproved");
            }
            var result = await _accountService.ConfirmEmailAsync(userId, code);

            if (result.Succeeded)
            {
                return new RedirectResult("/#/EmailApproved");
            }

            return new RedirectResult("/#/EmailUnapproved");
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryTokenERC]
        public async Task<ActionResult> ForgotPassword(ForgotPasswordViewModel model)
        {
            if (ReCAPTCHA(model.ReCaptcha) == true)
            {
                if (ModelState.IsValid)
                {
                    var user = await _accountService.FindByEmailAsync(model.Email);
                    if (user == null || !(await _accountService.IsEmailConfirmedAsync(user.Id)))
                    {
                        var badEmail = new IdentityResult("Email niepoprawny.");
                        AddErrors(badEmail);
                        return Json(IdentityResult.Failed(ModelState.Values.SelectMany(m => m.Errors).Select(e => e.ErrorMessage).ToArray()));
                    }

                    string code = await _accountService.GeneratePasswordResetTokenAsync(user.Id);
                    var callbackUrl = Url.Action("ResetPassword", "Account", new { userId = user.Id, code }, protocol: Request.Url.Scheme);
                    await _accountService.SendForgotPasswordEmailAsync(user.Id, callbackUrl, user, this);

                    return Json(IdentityResult.Success);
                }
            }
            else
            {
                var ReCAPTCHAFailResult = new IdentityResult("Należy zaznaczyć 'Nie jestem robotem'");
                AddErrors(ReCAPTCHAFailResult);

                return Json(IdentityResult.Failed(ModelState.Values.SelectMany(m => m.Errors).Select(e => e.ErrorMessage).ToArray()));
            }

            return Json(IdentityResult.Failed(ModelState.Values.SelectMany(m => m.Errors).Select(e => e.ErrorMessage).ToArray()));
        }

        [AllowAnonymous]
        public ActionResult ResetPassword(string code)
        {
            if (string.IsNullOrEmpty(code))
            {
                return new RedirectResult("/#/BadCode");
            }

            HttpCookie cookie = new HttpCookie("resetCode")
            {
                Value = code,
                Expires = DateTime.Now.AddHours(1)
            };
            Response.Cookies.Add(cookie);

            return new RedirectResult("/#/ResetPassword");
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryTokenERC]
        public async Task<ActionResult> ResetPassword(ResetPasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return Json(IdentityResult.Failed(ModelState.Values.SelectMany(m => m.Errors).Select(e => e.ErrorMessage).ToArray()));
            }
            var user = await _accountService.FindByEmailAsync(model.Email);
            if (user == null)
            {
                return Json(IdentityResult.Failed("Nie poprawny e-mail."));
            }
            var result = await _accountService.ResetPasswordAsync(user.Id, model.Code, model.Password);
            if (result.Succeeded)
            {
                return Json(IdentityResult.Success);
            }
            AddErrors(result);

            return Json(IdentityResult.Failed(ModelState.Values.SelectMany(m => m.Errors).Select(e => e.ErrorMessage).ToArray()));
        }

        [HttpPost]
        public void LogOff()
        {
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            AuthenticationManager.SignOut(OAuthDefaults.AuthenticationType);
        }

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        private bool ReCAPTCHA(string reCaptchaResponse)
        {
            string recaptchaKey = ConfigurationManager.AppSettings["reCAPTCHA"];
            var resultReCaptcha = new WebClient().DownloadString(
                string.Format("https://www.google.com/recaptcha/api/siteverify?secret={0}&response={1}", recaptchaKey, reCaptchaResponse));
            var reCaptchaResult = JObject.Parse(resultReCaptcha);

            return (bool)reCaptchaResult.SelectToken("success");
        }
    }
}