﻿using EastRidersWithAngular.Services.NewsService;
using EastRidersWithAngular.ViewModels.ErrorsViewModel;
using EastRidersWithAngular.ViewModels.NewsViewModel;
using System.Web.Mvc;

namespace EastRidersWithAngular.Controllers
{
    [Authorize]
    public class NewsController : Controller
    {
        private readonly INewsService _newsService;

        public NewsController(INewsService newsService)
        {
            _newsService = newsService;
        }

        [HttpGet]
        [AllowAnonymous]
        public ActionResult GetNewsByLimit(int limit)
        {
            var model = _newsService.GetNewsLimit(limit);

            return Json(model, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [Authorize(Roles = "Admin,Moderator")]
        public ActionResult GetNews()
        {
            var model = _newsService.GetNews();

            return Json(model, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [Authorize(Roles = "Admin,Moderator")]
        public ActionResult AddNews(NewsViewModel model)
        {
            model.CreatedBy = User.Identity.Name;
            int result = _newsService.AddNews(model);

            if (result == 0)
            {
                return Json(new ErrorViewModel { Succeeded = false, Errors = new string[] { "Coś poszło nie tak" } });
            }

            return Json(new ErrorViewModel { Succeeded = true });
        }

        [HttpPut]
        [Authorize(Roles = "Admin,Moderator")]
        public ActionResult UpdateNews(NewsViewModel model)
        {
            model.CreatedBy = User.Identity.Name;
            int result = _newsService.UpdateNews(model);

            if (result == 0)
            {
                return Json(new ErrorViewModel { Succeeded = false, Errors = new string[] { "Coś poszło nie tak" } });
            }

            return Json(new ErrorViewModel { Succeeded = true });
        }

        [HttpDelete]
        [Authorize(Roles = "Admin,Moderator")]
        public ActionResult DeleteNews(int id)
        {
            var result = _newsService.DeleteNews(id);

            if (result == 0)
            {
                return Json(new ErrorViewModel { Succeeded = false, Errors = new string[] { "Coś poszło nie tak" } });
            }

            return Json(new ErrorViewModel { Succeeded = true });
        }
    }
}