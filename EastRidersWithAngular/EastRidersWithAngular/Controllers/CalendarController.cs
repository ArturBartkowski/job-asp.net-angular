﻿using EastRidersWithAngular.Services.CalendarService;
using EastRidersWithAngular.ViewModels.CalendarViewModel;
using EastRidersWithAngular.ViewModels.ErrorsViewModel;
using System.Web.Mvc;

namespace EastRidersWithAngular.Controllers
{
    [Authorize]
    public class CalendarController : Controller
    {
        private readonly ICalendarService _calendarService;

        public CalendarController(ICalendarService calendarService)
        {
            _calendarService = calendarService;
        }

        [HttpGet]
        [AllowAnonymous]
        public ActionResult GetCalendarEvents()
        {
            var model = _calendarService.GetEvents();

            return Json(model, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        [Authorize(Roles = "Admin,Moderator")]
        public ActionResult AddCalendarEvent(EventInput model)
        {
            model.CreatedBy = User.Identity.Name;
            int result = _calendarService.AddEvent(model);

            if (result == 0)
            {
                return Json(new ErrorViewModel { Succeeded = false, Errors = new string[] { "Coś poszło nie tak" } });
            }

            return Json(new ErrorViewModel { Succeeded = true });
        }

        [HttpPut]
        [Authorize(Roles = "Admin,Moderator")]
        public ActionResult UpdateCalendarEvent(EventInput model)
        {
            model.CreatedBy = User.Identity.Name;
            int result = _calendarService.UpdateEvent(model);

            if (result == 0)
            {
                return Json(new ErrorViewModel { Succeeded = false, Errors = new string[] { "Coś poszło nie tak" } });
            }

            return Json(new ErrorViewModel { Succeeded = true });
        }

        [HttpDelete]
        [Authorize(Roles = "Admin,Moderator")]
        public ActionResult DeleteCalendarEvent(int id)
        {
            var result = _calendarService.DeleteEvent(id);

            if (result == 0)
            {
                return Json(new ErrorViewModel { Succeeded = false, Errors = new string[] { "Coś poszło nie tak" } });
            }

            return Json(new ErrorViewModel { Succeeded = true });
        }
    }
}