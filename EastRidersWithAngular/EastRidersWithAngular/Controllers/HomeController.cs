﻿using System.Web.Mvc;

namespace EastRidersWithAngular.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            return View();
        }
    }
}