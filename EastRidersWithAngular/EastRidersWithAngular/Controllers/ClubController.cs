﻿using EastRidersWithAngular.Services.ClubService;
using EastRidersWithAngular.ViewModels.ClubViewModel;
using EastRidersWithAngular.ViewModels.ErrorsViewModel;
using System.Collections.Generic;
using System.Web.Mvc;

namespace EastRidersWithAngular.Controllers
{
    [Authorize]
    public class ClubController : Controller
    {
        private readonly IClubService _clubService;

        public ClubController(IClubService clubService)
        {
            _clubService = clubService;
        }

        [HttpGet]
        [AllowAnonymous]
        public ActionResult GetClubDescription()
        {
            var model = _clubService.GetClubDescription();

            return Json(model, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [Authorize(Roles = "Member")]
        public ActionResult GetBasicClubInformation()
        {
            var basicModel = new List<ClubBasicInformationViewModel>()
            {
                new ClubBasicInformationViewModel()
                {
                    UserName = User.Identity.Name
                }
            };
            return Json(basicModel, JsonRequestBehavior.AllowGet);
        }

        [HttpPut]
        [Authorize(Roles = "Admin,Moderator")]
        public ActionResult UpdateClubInformation(ClubViewModel model)
        {
            int result = _clubService.UpdateClubInformation(model);

            if (result == 0)
            {
                return Json(new ErrorViewModel { Succeeded = false, Errors = new string[] { "Coś poszło nie tak" } });
            }

            return Json(new ErrorViewModel { Succeeded = true });
        }
    }
}