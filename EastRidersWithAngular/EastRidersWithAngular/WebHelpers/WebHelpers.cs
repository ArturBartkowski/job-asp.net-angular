﻿using Microsoft.AspNet.Identity;
using System.Configuration;
using System.Net.Configuration;
using System.Net.Mail;
using System.Net.Mime;
using System.Web;

namespace EastRidersWithAngular.WebHelpers
{
    public class WebHelpersERC
    {
        public MailMessage GetMailWithImg(IdentityMessage message, string[] paths)
        {
            SmtpSection section = (SmtpSection)ConfigurationManager.GetSection("system.net/mailSettings/smtp");

            MailMessage mailMessage = new MailMessage(section.From, message.Destination, message.Subject, message.Body)
            {
                IsBodyHtml = true
            };
            for (int i = 0; i < paths.Length; i++)
            {
                mailMessage.AlternateViews.Add(GetEmbeddedImage(paths[i], i, message.Body));
            }

            return mailMessage;
        }

        public AlternateView GetEmbeddedImage(string filePath, int iterator, string htmlBody)
        {
            var path = HttpContext.Current.Server.MapPath(@"~/Scripts/" + filePath);
            AlternateView alternateView = AlternateView.CreateAlternateViewFromString(htmlBody, null, MediaTypeNames.Text.Html);
            LinkedResource linkedResource = new LinkedResource(path)
            {
                ContentId = iterator.ToString()
            };
            alternateView.LinkedResources.Add(linkedResource);
            return alternateView;
        }
    }
}