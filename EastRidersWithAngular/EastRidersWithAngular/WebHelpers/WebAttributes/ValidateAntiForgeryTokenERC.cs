﻿using System;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web.Helpers;
using System.Web.Mvc;

namespace EastRidersWithAngular.WebAttributes
{
    public class ValidateAntiForgeryTokenERC : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext actionContext)
        {
            if (actionContext == null)
            {
                throw new ArgumentNullException("actionContext");
            }

            if (actionContext.HttpContext.Request.RequestType != "GET")
            {
                var headers = actionContext.HttpContext.Request.Headers;
                var tokenCookies = headers.GetValues("Cookie").FirstOrDefault();
                var tokenCookie = Regex.Split(tokenCookies, ";").FirstOrDefault().Replace("__RequestVerificationToken=", "");

                var tokenHeader = string.Empty;
                if (headers.AllKeys.Contains("X-XSRF-Token"))
                {
                    tokenHeader = headers.GetValues("X-XSRF-Token").FirstOrDefault();
                }

                AntiForgery.Validate(tokenCookie ?? null, tokenHeader);
            }

            base.OnActionExecuting(actionContext);
        }
    }
}