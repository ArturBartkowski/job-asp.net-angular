﻿using EastRiders.BL.DAOs.TokenDAO;
using EastRiders.BL.DatabaseLogic;
using EastRiders.BL.Entities;
using Microsoft.Owin.Security.Infrastructure;
using System;
using System.Configuration;
using System.Threading.Tasks;

namespace EastRidersWithAngular.Providers.Auth
{
    public class RefreshTokenProvider : IAuthenticationTokenProvider
    {
        private readonly ITokenRepository _tokenRepository;

        public RefreshTokenProvider(DatabaseContext context)
        {
            _tokenRepository = new TokenDAO(context);
        }

        public async Task CreateAsync(AuthenticationTokenCreateContext context)
        {
            var userId = context.Ticket.Properties.Dictionary["userId"];

            if (string.IsNullOrEmpty(userId))
            {
                return;
            }

            var refreshTokenId = Guid.NewGuid().ToString("n");

            var token = new RefreshToken()
            {
                IssuedUtc = DateTime.UtcNow,
                ExpiresUtc = DateTime.UtcNow.AddMinutes(Convert.ToDouble(ConfigurationManager.AppSettings["RefreshTokenExpireTimeMinutes"])),
                Token = refreshTokenId,
                UserId = userId
            };

            context.Ticket.Properties.IssuedUtc = token.IssuedUtc;
            context.Ticket.Properties.ExpiresUtc = token.ExpiresUtc;

            token.ProtectedTicket = context.SerializeTicket();

            var result = await _tokenRepository.AddRefreshToken(token);

            if (result)
            {
                context.SetToken(refreshTokenId);
            }
        }

        public async Task ReceiveAsync(AuthenticationTokenReceiveContext context)
        {
            var refreshToken = await _tokenRepository.FindRefreshTokenAsync(context.Token);

            if (refreshToken != null)
            {
                context.DeserializeTicket(refreshToken.ProtectedTicket);
                var result = await _tokenRepository.RemoveRefreshToken(context.Token);
            }
        }

        public void Create(AuthenticationTokenCreateContext context)
        {
            throw new NotImplementedException();
        }

        public void Receive(AuthenticationTokenReceiveContext context)
        {
            throw new NotImplementedException();
        }
    }
}