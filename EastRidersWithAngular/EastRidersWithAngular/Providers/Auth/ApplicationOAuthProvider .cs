﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.OAuth;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace EastRidersWithAngular.Providers.Auth
{
    public class ApplicationOAuthProvider : OAuthAuthorizationServerProvider
    {
        private readonly string _publicClientId;

        public ApplicationOAuthProvider(string publicClientId)
        {
            _publicClientId = publicClientId;
        }

        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            try
            {
                var userManager = context.OwinContext.GetUserManager<ApplicationUserManager>();
                var user = await userManager.FindAsync(context.UserName, context.Password);

                if (user == null)
                {
                    context.SetError("Error", "Nazwa użytkownika lub hasło jest niepoprawne.");
                    return;
                }

                if (!user.EmailConfirmed)
                {
                    context.SetError("Error", "E-mail nie został potwierdzony.");
                    return;
                }

                var roles = userManager.GetRoles(user.Id);

                ClaimsIdentity oAuthIdentity = await user.GenerateUserIdentityAsync(userManager, OAuthDefaults.AuthenticationType);
                ClaimsIdentity cookiesIdentity = await user.GenerateUserIdentityAsync(userManager, CookieAuthenticationDefaults.AuthenticationType);

                AuthenticationProperties properties = CreateProperties(user.UserName, user.Id, roles);

                AuthenticationTicket ticket = new AuthenticationTicket(oAuthIdentity, properties);
                context.Validated(ticket);
                context.Request.Context.Authentication.SignIn(cookiesIdentity);
            }
            catch (Exception)
            {
                context.SetError("Critical Error", "Błąd krytyczny w czasie logowania.");
            }
        }

        public override Task GrantRefreshToken(OAuthGrantRefreshTokenContext context)
        {
            var userId = context.Ticket.Properties.Dictionary["userId"];
            if (string.IsNullOrEmpty(userId))
            {
                context.SetError("Error", "Identyfikator użytkownika jest niepoprawny.");
                return Task.FromResult<object>(null);
            }

            var userManager = context.OwinContext.GetUserManager<ApplicationUserManager>();
            var user = userManager.Users.Single(i => i.Id == userId);

            if (user == null)
            {
                context.SetError("Error", "Nazwa użytkownika lub hasło jest niepoprawne.");
                return Task.FromResult<object>(null);
            }

            if (!user.EmailConfirmed)
            {
                context.SetError("Error", "E-mail nie został potwierdzony.");
                return Task.FromResult<object>(null);
            }

            var newIdentity = new ClaimsIdentity(context.Ticket.Identity);
            newIdentity.AddClaim(new Claim("newClaim", "newValue"));

            var newTicket = new AuthenticationTicket(newIdentity, context.Ticket.Properties);
            context.Validated(newTicket);

            return Task.FromResult<object>(null);
        }

        public override Task TokenEndpoint(OAuthTokenEndpointContext context)
        {
            foreach (var property in context.Properties.Dictionary)
            {
                context.AdditionalResponseParameters.Add(property.Key, property.Value);
            }

            return Task.FromResult(0);
        }

        public override Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            if (context.ClientId == null)
            {
                context.Validated();
            }

            return Task.FromResult(0);
        }

        public override Task ValidateClientRedirectUri(OAuthValidateClientRedirectUriContext context)
        {
            if (context.ClientId == _publicClientId)
            {
                Uri expectedRootUri = new Uri(context.Request.Uri, "/");

                if (expectedRootUri.AbsoluteUri == context.RedirectUri)
                {
                    context.Validated();
                }
            }

            return Task.FromResult(0);
        }

        public static AuthenticationProperties CreateProperties(string userName, string userId, IEnumerable<string> roles)
        {
            var data = new Dictionary<string, string>
            {
                { "userName", userName },
                { "userId", userId },
                { "roles", string.Join(",", roles.Select(x => x.ToLower())) },
            };

            return new AuthenticationProperties(data);
        }
    }
}