﻿using System;
using System.Collections.Generic;
using System.Web;
using AutoMapper;
using EastRiders.BL.DAOs.GalleryDAO;
using EastRiders.BL.Entities;
using EastRidersWithAngular.ViewModels.GalleryViewModel;
using Newtonsoft.Json;
using RestSharp;

namespace EastRidersWithAngular.Services.GalleryService
{
    public class GalleryService : IGalleryService
    {
        private readonly IGalleryRepository _galleryRepository;

        public GalleryService(IGalleryRepository galleryRepository)
        {
            _galleryRepository = galleryRepository;
        }

        public IEnumerable<GalleryViewModel> GetGalleriesByLimit(int limit)
        {
            return Mapper.Map<IEnumerable<GalleryViewModel>>(_galleryRepository.GetGalleryByLimit(limit));
        }

        public AlbumsJsonModel GetAlbumsFromGooglePhotos(string authToken)
        {
            string nextPageToken = null;
            int counter = 0;
            var clientAlbum = new RestClient("https://photoslibrary.googleapis.com/v1/albums?pageSize=50");
            var requestAlbum = new RestRequest(Method.GET);
            requestAlbum.AddHeader("Authorization", "Bearer " + authToken);
            IRestResponse response = clientAlbum.Execute(requestAlbum);
            AlbumsJsonModel albumsViewModel = new AlbumsJsonModel();
            albumsViewModel = JsonConvert.DeserializeObject<AlbumsJsonModel>(response.Content);

            if (albumsViewModel.NextPageToken != null)
            {
                nextPageToken = albumsViewModel.NextPageToken;
                counter++;
            }

            for (int i = 0; i < counter; i++)
            {
                var clientNextPage = new RestClient("https://photoslibrary.googleapis.com/v1/albums?pageSize=50&pageToken=" + @nextPageToken);
                var requestNextPage = new RestRequest(Method.GET);
                requestNextPage.AddHeader("Authorization", "Bearer " + authToken);
                IRestResponse responseNextPage = clientNextPage.Execute(requestNextPage);
                AlbumsJsonModel albumsViewModelNextPage = new AlbumsJsonModel();
                albumsViewModelNextPage = JsonConvert.DeserializeObject<AlbumsJsonModel>(responseNextPage.Content);
                albumsViewModel.Albums.AddRange(albumsViewModelNextPage.Albums);

                if (albumsViewModelNextPage.NextPageToken != null)
                {
                    nextPageToken = albumsViewModelNextPage.NextPageToken;
                    counter++;
                }
            }

            return albumsViewModel;
        }

        public List<GalleryJsonModel> GetImagesToAlbumsFromGooglePhotos(string authToken, AlbumsJsonModel model)
        {
            var galleryList = new List<GalleryJsonModel>();
            foreach (var album in model.Albums)
            {
                string nextPageToken = null;
                int counter = 0;
                var clientImages = new RestClient("https://photoslibrary.googleapis.com/v1/mediaItems:search?pageSize=100&albumId=" + album.Id);
                var requestImages = new RestRequest(Method.POST);
                requestImages.AddHeader("Authorization", "Bearer " + authToken);
                IRestResponse response = clientImages.Execute(requestImages);
                ImagesJsonModel imagesJsonModel = new ImagesJsonModel();
                imagesJsonModel = JsonConvert.DeserializeObject<ImagesJsonModel>(response.Content);

                if (imagesJsonModel.NextPageToken != null)
                {
                    nextPageToken = imagesJsonModel.NextPageToken;
                    counter++;
                }

                for (int i = 0; i < counter; i++)
                {
                    var clientNextPage = new RestClient("https://photoslibrary.googleapis.com/v1/mediaItems:search?pageSize=100&pageToken=" + @nextPageToken);
                    var requestNextPage = new RestRequest(Method.POST);
                    requestNextPage.AddHeader("Authorization", "Bearer " + authToken);
                    IRestResponse responseNextPage = clientNextPage.Execute(requestNextPage);
                    ImagesJsonModel albumsViewModelNextPage = new ImagesJsonModel();
                    albumsViewModelNextPage = JsonConvert.DeserializeObject<ImagesJsonModel>(responseNextPage.Content);
                    imagesJsonModel.MediaItems.AddRange(albumsViewModelNextPage.MediaItems);

                    if (albumsViewModelNextPage.NextPageToken != null)
                    {
                        nextPageToken = albumsViewModelNextPage.NextPageToken;
                        counter++;
                    }
                }

                var singlegallery = new GalleryJsonModel()
                {
                    AlbumId = album.Id,
                    CreatedBy = HttpContext.Current.User.Identity.Name,
                    CreatedDate = DateTime.Now,
                    EventDate = imagesJsonModel.MediaItems[0].MediaMetadata.CreationTime,
                    Title = album.Title,
                    Images = new List<ImageJsonModel>()
                };

                singlegallery.Images.AddRange(imagesJsonModel.MediaItems);
                galleryList.Add(singlegallery);
            }

            return galleryList;
        }

        public int SaveAlbums(List<GalleryJsonModel> model)
        {
            return _galleryRepository.AddGallery(Mapper.Map<IEnumerable<Gallery>>(model));
        }
    }
}