﻿using EastRidersWithAngular.ViewModels.GalleryViewModel;
using System.Collections.Generic;

namespace EastRidersWithAngular.Services.GalleryService
{
    public interface IGalleryService
    {
        IEnumerable<GalleryViewModel> GetGalleriesByLimit(int limit);
        AlbumsJsonModel GetAlbumsFromGooglePhotos(string authToken);
        List<GalleryJsonModel> GetImagesToAlbumsFromGooglePhotos(string authToken, AlbumsJsonModel model);
        int SaveAlbums(List<GalleryJsonModel> model);
    }
}
