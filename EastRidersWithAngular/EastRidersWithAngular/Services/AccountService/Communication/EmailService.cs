﻿using EastRidersWithAngular.WebHelpers;
using Microsoft.AspNet.Identity;
using System.Net.Mail;
using System.Threading.Tasks;

namespace EastRidersWithAngular.Services.AccountService
{
    public class EmailService : IIdentityMessageService
    {
        //Refactoring
        public Task SendAsync(IdentityMessage message)
        {
            SmtpClient client = new SmtpClient();
            WebHelpersERC webHelpers = new WebHelpersERC();
            MailMessage mailMessage = webHelpers.GetMailWithImg(message, new string[]
            { "rounder-up.png","logo-2.png", "rounder-dwn.png" });

            return client.SendMailAsync(mailMessage);
        }
    }
}