﻿using EastRiders.BL.DatabaseLogic;
using EastRidersWithAngular.Controllers;
using EastRidersWithAngular.Providers.Auth;
using EastRidersWithAngular.ViewModels.AccountViewModel;
using EastRidersWithAngular.ViewModels.Models;
using EastRidersWithAngular.WebHelpers.EMailTemplates;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Configuration;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;

namespace EastRidersWithAngular.Services.AccountService
{
    public class AccountService : IAccountService
    {
        private readonly ApplicationSignInManager _signInManager;
        private readonly ApplicationUserManager _userManager;

        public AccountService(ApplicationSignInManager applicationSignInManager, ApplicationUserManager applicationUserManager)
        {
            _signInManager = applicationSignInManager;
            _userManager = applicationUserManager;
        }

        public async Task<SignInStatus> Login(LoginViewModel model)
        {
            return await _signInManager.PasswordSignInAsync(model.UserName, model.Password, model.RememberMe, shouldLockout: false);
        }

        public async Task<bool> HasBeenVerifiedAsync()
        {
            return await _signInManager.HasBeenVerifiedAsync();
        }

        public async Task<SignInStatus> TwoFactorSignInAsync(VerifyCodeViewModel model)
        {
            return await _signInManager.TwoFactorSignInAsync(model.Provider, model.Code, isPersistent: model.RememberMe, rememberBrowser: model.RememberBrowser);
        }

        public async Task<IdentityResult> CreateAsyncAccount(RegisterViewModel model, ApplicationUser user)
        {
            return await _userManager.CreateAsync(user, model.Password);
        }

        public async Task SignInAsync(ApplicationUser user)
        {
            await _signInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
        }

        public async Task<string> GenerateEmailConfirmationTokenAsync(string userId)
        {
            return await _userManager.GenerateEmailConfirmationTokenAsync(userId);
        }

        public async Task SendConfirmEmailAsync(string userId, string callbackUrl, ApplicationUser user, AccountController controller)
        {
            RenderEmailViewModel confirmEmailViewModel = new RenderEmailViewModel()
            {
                Email = user.Email,
                Name = user.UserName,
                Link = callbackUrl
            };

            string bodyConfirmMail = RazorViewToString.RenderRazorViewToString(controller, "/Views/EMailTemplates/ConfirmEMail.cshtml", confirmEmailViewModel);

            await _userManager.SendEmailAsync(userId, "Potwierdź adres e-mail", bodyConfirmMail);
        }

        public async Task SendForgotPasswordEmailAsync(string userId, string callbackUrl, ApplicationUser user, AccountController controller)
        {
            RenderEmailViewModel confirmEmailViewModel = new RenderEmailViewModel()
            {
                Email = user.Email,
                Name = user.UserName,
                Link = callbackUrl
            };

            string bodyConfirmMail = RazorViewToString.RenderRazorViewToString(controller, "/Views/EMailTemplates/ForgotPasswordEMail.cshtml", confirmEmailViewModel);

            await _userManager.SendEmailAsync(userId, "Przywracanie hasła", bodyConfirmMail);
        }

        public async Task<IdentityResult> ConfirmEmailAsync(string userId, string code)
        {
            return await _userManager.ConfirmEmailAsync(userId, code);
        }

        public async Task<ApplicationUser> FindByEmailAsync(string Email)
        {
            return await _userManager.FindByEmailAsync(Email);
        }

        public async Task<bool> IsEmailConfirmedAsync(string userId)
        {
            return await _userManager.IsEmailConfirmedAsync(userId);
        }

        public async Task<IdentityResult> ResetPasswordAsync(string userId, string code, string password)
        {
            return await _userManager.ResetPasswordAsync(userId, code, password);
        }

        public async Task<string> GetVerifiedUserIdAsync()
        {
            return await _signInManager.GetVerifiedUserIdAsync();
        }

        public async Task<IList<string>> GetValidTwoFactorProvidersAsync(string userId)
        {
            return await _userManager.GetValidTwoFactorProvidersAsync(userId);
        }

        public async Task<bool> SendTwoFactorCodeAsync(string SelectedProvider)
        {
            return await _signInManager.SendTwoFactorCodeAsync(SelectedProvider);
        }

        public async Task<string> GeneratePasswordResetTokenAsync(string userId)
        {
            return await _userManager.GeneratePasswordResetTokenAsync(userId);
        }

        public async Task<TokenViewModel> GetJWTToken(LoginViewModel model)
        {
            HttpClient httpClient = new HttpClient();
            var content = new FormUrlEncodedContent(new[]
            {
                new KeyValuePair<string, string>("grant_type", "password"),
                new KeyValuePair<string, string>("username", model.UserName),
                new KeyValuePair<string, string>("password", model.Password)
            });

            HttpResponseMessage response = await httpClient.PostAsync(@ConfigurationManager.AppSettings["TokenApi"], content);
            var resultContent = await response.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<TokenViewModel>(resultContent);
        }

        public void Dispose()
        {
            _userManager.Dispose();
            _signInManager.Dispose();
        }
    }
}