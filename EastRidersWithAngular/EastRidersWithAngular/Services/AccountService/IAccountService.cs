﻿using EastRiders.BL.DatabaseLogic;
using EastRidersWithAngular.Controllers;
using EastRidersWithAngular.ViewModels.AccountViewModel;
using EastRidersWithAngular.ViewModels.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EastRidersWithAngular.Services.AccountService
{
    public interface IAccountService : IDisposable
    {
        Task<SignInStatus> Login(LoginViewModel model);
        Task<bool> HasBeenVerifiedAsync();
        Task<SignInStatus> TwoFactorSignInAsync(VerifyCodeViewModel model);
        Task<IdentityResult> CreateAsyncAccount(RegisterViewModel model, ApplicationUser user);
        Task SignInAsync(ApplicationUser user);
        Task<string> GenerateEmailConfirmationTokenAsync(string userId);
        Task<string> GeneratePasswordResetTokenAsync(string userId);
        Task SendConfirmEmailAsync(string userId, string callbackUrl, ApplicationUser user, AccountController controller);
        Task SendForgotPasswordEmailAsync(string userId, string callbackUrl, ApplicationUser user, AccountController controller);
        Task<IdentityResult> ConfirmEmailAsync(string userId, string code);
        Task<ApplicationUser> FindByEmailAsync(string Email);
        Task<bool> IsEmailConfirmedAsync(string userId);
        Task<IdentityResult> ResetPasswordAsync(string userId, string code, string password);
        Task<string> GetVerifiedUserIdAsync();
        Task<IList<string>> GetValidTwoFactorProvidersAsync(string userId);
        Task<bool> SendTwoFactorCodeAsync(string SelectedProvider);
        Task<TokenViewModel> GetJWTToken(LoginViewModel model);
    }
}
