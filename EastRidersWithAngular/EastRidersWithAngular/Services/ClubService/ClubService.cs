﻿using AutoMapper;
using EastRiders.BL.DAOs.ClubDAO;
using EastRiders.BL.Entities;
using EastRidersWithAngular.ViewModels.ClubViewModel;

namespace EastRidersWithAngular.Services.ClubService
{
    public class ClubService : IClubService
    {
        private readonly IClubRepository _clubrepository;

        public ClubService(IClubRepository clubRepository)
        {
            _clubrepository = clubRepository;
        }

        public ClubViewModel GetClubDescription()
        {
            return Mapper.Map<ClubViewModel>(_clubrepository.GetClubDescription());
        }

        public int UpdateClubInformation(ClubViewModel club)
        {
            return _clubrepository.Update(Mapper.Map<Club>(club));
        }
    }
}