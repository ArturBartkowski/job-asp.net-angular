﻿using EastRidersWithAngular.ViewModels.ClubViewModel;

namespace EastRidersWithAngular.Services.ClubService
{
    public interface IClubService
    {
        ClubViewModel GetClubDescription();
        int UpdateClubInformation(ClubViewModel club);
    }
}
