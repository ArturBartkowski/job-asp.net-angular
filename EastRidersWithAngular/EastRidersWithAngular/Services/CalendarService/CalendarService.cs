﻿using System;
using System.Collections.Generic;
using AutoMapper;
using EastRiders.BL.DAOs.CalendarDAO;
using EastRiders.BL.Entities;
using EastRidersWithAngular.ViewModels.CalendarViewModel;

namespace EastRidersWithAngular.Services.CalendarService
{
    public class CalendarService : ICalendarService
    {
        private readonly ICalendarRepository _calendarRepository;

        public CalendarService(ICalendarRepository calendarRepository)
        {
            _calendarRepository = calendarRepository;
        }

        public int DeleteEvent(int id)
        {
            return _calendarRepository.DeleteEvent(id);
        }

        public IEnumerable<EventInput> GetEvents()
        {
            return Mapper.Map<IEnumerable<EventInput>>(_calendarRepository.GetEvents());
        }

        public int AddEvent(EventInput model)
        {
            model.CreatedDate = DateTime.Now;
            return _calendarRepository.AddEvent(Mapper.Map<Calendar>(model));
        }

        public int UpdateEvent(EventInput model)
        {
            model.CreatedDate = DateTime.Now;
            return _calendarRepository.UpdateEvent(Mapper.Map<Calendar>(model));
        }
    }
}