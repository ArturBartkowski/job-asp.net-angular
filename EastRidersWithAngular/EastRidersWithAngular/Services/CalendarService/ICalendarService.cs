﻿using EastRidersWithAngular.ViewModels.CalendarViewModel;
using System.Collections.Generic;

namespace EastRidersWithAngular.Services.CalendarService
{
    public interface ICalendarService
    {
        int AddEvent(EventInput calendar);
        IEnumerable<EventInput> GetEvents();
        int UpdateEvent(EventInput news);
        int DeleteEvent(int id);
    }
}
