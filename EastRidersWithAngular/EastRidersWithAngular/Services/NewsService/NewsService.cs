﻿using System;
using System.Collections.Generic;
using AutoMapper;
using EastRiders.BL.DAOs.NewsDAO;
using EastRiders.BL.Entities;
using EastRidersWithAngular.ViewModels.NewsViewModel;

namespace EastRidersWithAngular.Services.NewsService
{
    public class NewsService : INewsService
    {
        private readonly INewsRepository _newsRepository;

        public NewsService(INewsRepository newsRepository)
        {
            _newsRepository = newsRepository;
        }

        public int AddNews(NewsViewModel model)
        {
            model.CreationDate = DateTime.Now;
            return _newsRepository.AddNews(Mapper.Map<News>(model));
        }

        public int DeleteNews(int newsId)
        {
            return _newsRepository.DeleteNews(newsId);
        }

        public IEnumerable<NewsViewModel> GetNews()
        {
            return Mapper.Map<IEnumerable<NewsViewModel>>(_newsRepository.GetNews());
        }

        public IEnumerable<NewsViewModel> GetNewsLimit(int limit)
        {
            return Mapper.Map<IEnumerable<NewsViewModel>>(_newsRepository.GetFirstsNewsByLimit(limit));
        }

        public int UpdateNews(NewsViewModel model)
        {
            model.CreationDate = DateTime.Now;
            return _newsRepository.UpdateNews(Mapper.Map<News>(model));
        }
    }
}