﻿using EastRidersWithAngular.ViewModels.NewsViewModel;
using System.Collections.Generic;

namespace EastRidersWithAngular.Services.NewsService
{
    public interface INewsService
    {
        IEnumerable<NewsViewModel> GetNewsLimit(int limit);
        int AddNews(NewsViewModel model);
        int UpdateNews(NewsViewModel model);
        int DeleteNews(int newsId);
        IEnumerable<NewsViewModel> GetNews();
    }
}
