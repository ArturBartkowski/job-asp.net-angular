﻿namespace EastRidersWithAngular.ViewModels.ErrorsViewModel
{
    public class ErrorViewModel
    {
        public bool Succeeded { get; set; }
        public string[] Errors { get; set; }
    }
}