﻿namespace EastRidersWithAngular.ViewModels.ClubViewModel
{
    public class ClubViewModel
    {
        public int Id { get; set; }
        public string Description { get; set; }       
        public string President { get; set; }    
        public string VPresidentSecretary { get; set; }      
        public string VPresident { get; set; }     
        public string Treasurer { get; set; }       
        public string Photographer { get; set; }      
        public string GraphicDesigner { get; set; }   
        public string Chairman { get; set; }     
        public string Member1 { get; set; }     
        public string Member2 { get; set; }
    }
}