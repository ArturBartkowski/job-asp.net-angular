﻿namespace EastRidersWithAngular.ViewModels.AccountViewModel
{
    public class RenderEmailViewModel
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public string Link { get; set; }
    }
}