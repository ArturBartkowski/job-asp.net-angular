﻿namespace EastRidersWithAngular.ViewModels.AccountViewModel
{
    public class TokenViewModel
    {
        public string Access_token { get; set; }
        public string Refresh_token { get; set; }
        public string Error_description { get; set; }
    }
}