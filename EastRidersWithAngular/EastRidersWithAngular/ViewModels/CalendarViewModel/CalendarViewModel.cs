﻿using System;

namespace EastRidersWithAngular.ViewModels.CalendarViewModel
{
    public class EventInput
    {
        public string id { get; set; }
        public string title { get; set; }
        public bool allDay { get; set; }
        public string backgroundColor { get; set; }
        public string borderColor { get; set; }
        public string end { get; set; }
        public string start { get; set; }
        public string textColor { get; set; }
        public string url { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}