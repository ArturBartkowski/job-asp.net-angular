﻿using System;

namespace EastRidersWithAngular.ViewModels.NewsViewModel
{
    public class NewsViewModel
    {
        public int Id { get; set; }
        public string Url { get; set; }
        public DateTime CreationDate { get; set; }
        public string CreatedBy { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string ShortDescription { get; set; }
        public string Type { get; set; }
    }
}