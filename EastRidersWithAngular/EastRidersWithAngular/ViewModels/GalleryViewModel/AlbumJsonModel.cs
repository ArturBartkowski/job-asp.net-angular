﻿using System.Collections.Generic;

namespace EastRidersWithAngular.ViewModels.GalleryViewModel
{
    public class AlbumJsonModel
    {
        public string Id { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedDate { get; set; }
        public string EventDate { get; set; }
        public string Title { get; set; }
    }

    public class AlbumsJsonModel
    {
        public List<AlbumJsonModel> Albums { get; set; }
        public string NextPageToken { get; set; }
    }
}