﻿using System;
using System.Collections.Generic;

namespace EastRidersWithAngular.ViewModels.GalleryViewModel
{
    public class ImageJsonModel
    {
        public string BaseUrl { get; set; }
        public string MimeType { get; set; }
        public string FileName { get; set; }
        public MediaMetadata MediaMetadata { get; set; }
    }

    public class ImagesJsonModel
    {
        public List<ImageJsonModel> MediaItems { get; set; }
        public string NextPageToken { get; set; }
    }

    public class MediaMetadata
    {
        public DateTime CreationTime { get; set; }
    }
}