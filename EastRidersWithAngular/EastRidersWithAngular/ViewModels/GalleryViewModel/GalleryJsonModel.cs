﻿using System;
using System.Collections.Generic;

namespace EastRidersWithAngular.ViewModels.GalleryViewModel
{
    public class GalleryJsonModel
    {
        public string AlbumId { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime EventDate { get; set; }
        public string Title { get; set; }

        public List<ImageJsonModel> Images { get; set; }
    }
}