﻿using System;
using System.Collections.Generic;

namespace EastRidersWithAngular.ViewModels.GalleryViewModel
{
    public class GalleryViewModel
    {
        public string CreatedBy { get; set; }
        public DateTime EventDate { get; set; }
        public string Title { get; set; }
        public IEnumerable<ImageViewModel> Images { get; set; }
    }
}