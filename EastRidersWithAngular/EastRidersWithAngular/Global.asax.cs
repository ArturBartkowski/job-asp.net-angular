﻿using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Optimization;
using System.Web;

namespace EastRidersWithAngular
{
    public class MvcApplication : HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            App_Start.AutoMapperConfig.Initialize();
        }
    }
}
