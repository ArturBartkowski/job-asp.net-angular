﻿using System.Web.Optimization;

namespace EastRidersWithAngular
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/Script/Bundles").Include(
                       "~/Scripts/Bundles/vendor.*",
                       "~/Scripts/Bundles/runtime.*",
                       "~/Scripts/Bundles/zone.*",
                       "~/Scripts/Bundles/polyfills.*",
                       "~/Scripts/Bundles/main.*"));

            bundles.Add(new ScriptBundle("~/Script/Css").Include(
            "~/Scripts/Css/MainPageCss.css"));
        }
    }
}