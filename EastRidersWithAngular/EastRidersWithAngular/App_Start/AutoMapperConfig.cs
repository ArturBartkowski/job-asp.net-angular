﻿using AutoMapper;
using EastRiders.BL.Entities;
using EastRidersWithAngular.ViewModels.CalendarViewModel;
using EastRidersWithAngular.ViewModels.ClubViewModel;
using EastRidersWithAngular.ViewModels.GalleryViewModel;
using EastRidersWithAngular.ViewModels.NewsViewModel;

namespace EastRidersWithAngular.App_Start
{
    public class AutoMapperConfig
    {
        public static void Initialize()
        {
            Mapper.Initialize(cfg =>
            {
                cfg.CreateMap<News, NewsViewModel>();
                cfg.CreateMap<Club, ClubViewModel>();
                cfg.CreateMap<Calendar, EventInput>();
                cfg.CreateMap<EventInput, Calendar>();
                cfg.CreateMap<Gallery, GalleryViewModel>();
                cfg.CreateMap<Images, ImageViewModel>();
                cfg.CreateMap<GalleryJsonModel, Gallery>();
                cfg.CreateMap<ImageJsonModel, Images>();
            });
        }
    }
}