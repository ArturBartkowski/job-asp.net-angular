﻿[assembly: WebActivatorEx.PreApplicationStartMethod(typeof(EastRidersWithAngular.App_Start.NinjectWebCommon), "Start")]
[assembly: WebActivatorEx.ApplicationShutdownMethod(typeof(EastRidersWithAngular.App_Start.NinjectWebCommon), "Stop")]

namespace EastRidersWithAngular.App_Start
{
    using EastRiders.BL.DAOs.CalendarDAO;
    using EastRiders.BL.DAOs.ClubDAO;
    using EastRiders.BL.DAOs.GalleryDAO;
    using EastRiders.BL.DAOs.NewsDAO;
    using EastRiders.BL.DAOs.TokenDAO;
    using EastRidersWithAngular.Services.AccountService;
    using EastRidersWithAngular.Services.CalendarService;
    using EastRidersWithAngular.Services.ClubService;
    using EastRidersWithAngular.Services.GalleryService;
    using EastRidersWithAngular.Services.NewsService;
    using Microsoft.AspNet.Identity.Owin;
    using Microsoft.Owin.Security;
    using Microsoft.Web.Infrastructure.DynamicModuleHelper;
    using Ninject;
    using Ninject.Web.Common;
    using Ninject.Web.Common.WebHost;
    using System;
    using System.Web;

    public static class NinjectWebCommon
    {
        private static readonly Bootstrapper bootstrapper = new Bootstrapper();

        public static void Start()
        {
            DynamicModuleUtility.RegisterModule(typeof(OnePerRequestHttpModule));
            DynamicModuleUtility.RegisterModule(typeof(NinjectHttpModule));
            bootstrapper.Initialize(CreateKernel);
        }

        public static void Stop()
        {
            bootstrapper.ShutDown();
        }

        private static IKernel CreateKernel()
        {
            var kernel = new StandardKernel();
            try
            {
                kernel.Bind<Func<IKernel>>().ToMethod(ctx => () => new Bootstrapper().Kernel);
                kernel.Bind<IHttpModule>().To<HttpApplicationInitializationHttpModule>();
                RegisterServices(kernel);

                return kernel;
            }
            catch
            {
                kernel.Dispose();
                throw;
            }
        }

        private static void RegisterServices(IKernel kernel)
        {
            kernel.Bind<INewsRepository>().To<NewsDAO>();
            kernel.Bind<INewsService>().To<NewsService>();
            kernel.Bind<IClubRepository>().To<ClubDAO>();
            kernel.Bind<IClubService>().To<ClubService>();
            kernel.Bind<ICalendarRepository>().To<CalendarDAO>();
            kernel.Bind<ICalendarService>().To<CalendarService>();
            kernel.Bind<IGalleryRepository>().To<GalleryDAO>();
            kernel.Bind<IGalleryService>().To<GalleryService>();
            kernel.Bind<IAccountService>().To<AccountService>();
            kernel.Bind<ITokenRepository>().To<TokenDAO>();
            kernel.Bind<IAuthenticationManager>().ToMethod(c =>
                HttpContext.Current.GetOwinContext().Authentication).InRequestScope();
            kernel.Bind<ApplicationUserManager>().ToMethod(c =>
                HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>()).InRequestScope();
        }
    }
}