﻿using EastRiders.BL.Entities;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;

namespace EastRiders.BL.DatabaseLogic
{
    public class DatabaseContext : IdentityDbContext<ApplicationUser>
    {
        public DatabaseContext() : base("eastride_db", throwIfV1Schema: false) { }
        public DbSet<Images> Image { get; set; }
        public DbSet<Gallery> Gallery { get; set; }
        public DbSet<News> News { get; set; }
        public DbSet<Club> Club { get; set; }
        public DbSet<Calendar> Calendar { get; set; }
        public DbSet<RefreshToken> RefreshTokens { get; set; }

        public static DatabaseContext Create()
        {
            return new DatabaseContext();
        }
    }

    public class ApplicationUser : IdentityUser
    {
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager, string authenticationType)
        {
            var userIdentity = await manager.CreateIdentityAsync(this, authenticationType);
            return userIdentity;
        }
    }
}
