﻿using EastRiders.BL.Enums;
using System;
using System.ComponentModel.DataAnnotations;

namespace EastRiders.BL.Entities
{
    public class News
    {
        [Key]
        public int Id { get; set; }
        [StringLength(500)]
        public string Url { get; set; }
        public DateTime CreationDate { get; set; }
        [StringLength(30)]
        public string CreatedBy { get; set; }
        [StringLength(100)]
        public string Title { get; set; }
        [StringLength(200)]
        public string ShortDescription { get; set; }
        public string Description { get; set; }
        public CategoryNewsEnum Type { get; set; }
    }
}
