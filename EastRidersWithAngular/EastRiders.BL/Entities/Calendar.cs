﻿using System;
using System.ComponentModel.DataAnnotations;

namespace EastRiders.BL.Entities
{
    public class Calendar
    {
        [Key]
        public int Id { get; set; }
        [StringLength(200)]
        public string Title { get; set; }
        public bool AllDay { get; set; }
        public string Start { get; set; }
        public string End { get; set; }
        [StringLength(200)]
        public string Url { get; set; }
        [StringLength(20)]
        public string BackgroundColor { get; set; }
        [StringLength(20)]
        public string BorderColor { get; set; }
        [StringLength(20)]
        public string TextColor { get; set; }
        [StringLength(40)]
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}
