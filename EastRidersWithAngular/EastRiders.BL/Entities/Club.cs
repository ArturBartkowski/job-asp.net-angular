﻿using System.ComponentModel.DataAnnotations;

namespace EastRiders.BL.Entities
{
    public class Club
    {
        public int Id { get; set; }
        public string Description { get; set; }
        [StringLength(40)]
        public string President { get; set; }
        [StringLength(40)]
        public string VPresidentSecretary { get; set; }
        [StringLength(40)]
        public string VPresident { get; set; }
        [StringLength(40)]
        public string Treasurer { get; set; }
        [StringLength(40)]
        public string Photographer { get; set; }
        [StringLength(40)]
        public string GraphicDesigner { get; set; }
        [StringLength(40)]
        public string Chairman { get; set; }
        [StringLength(40)]
        public string Member1 { get; set; }
        [StringLength(40)]
        public string Member2 { get; set; }
    }
}
