﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace EastRiders.BL.Entities
{
    public class Gallery
    {
        [Key]
        public int GalleryId { get; set; }
        public string AlbumId { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime EventDate { get; set; }
        public string Title { get; set; }

        public virtual ICollection<Images> Images { get; set; }
    }
}
