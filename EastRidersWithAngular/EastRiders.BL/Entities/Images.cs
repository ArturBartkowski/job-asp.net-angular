﻿using System.ComponentModel.DataAnnotations;

namespace EastRiders.BL.Entities
{
    public class Images
    {
        [Key]
        public int Id { get; set; }
        public string BaseUrl { get; set; }
        public string MimeType { get; set; }
        public string FileName { get; set; }

        public int GalleryId { get; set; }
        public virtual Gallery Gallery { get; set; }
    }
}
