﻿using EastRiders.BL.Entities;
using System;

namespace EastRiders.BL.DAOs.ClubDAO
{
    public interface IClubRepository : IDisposable
    {
        Club GetClubDescription();
        int Update(Club club);
    }
}
