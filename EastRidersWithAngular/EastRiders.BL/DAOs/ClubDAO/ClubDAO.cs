﻿using EastRiders.BL.DatabaseLogic;
using EastRiders.BL.Entities;
using System.Data.Entity;
using System.Linq;

namespace EastRiders.BL.DAOs.ClubDAO
{
    public class ClubDAO : IClubRepository
    {
        private readonly DatabaseContext _context;

        public ClubDAO(DatabaseContext context)
        {
            _context = context;
        }

        public Club GetClubDescription()
        {
            return _context.Club.SingleOrDefault();
        }

        public int Update(Club club)
        {
            _context.Entry(club).State = EntityState.Modified;
            return _context.SaveChanges();
        }

        public void Dispose()
        {
            _context.Dispose();
        }
    }
}
