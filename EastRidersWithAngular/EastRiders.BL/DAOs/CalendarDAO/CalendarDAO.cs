﻿using System.Data.Entity;
using System.Linq;
using EastRiders.BL.DatabaseLogic;
using EastRiders.BL.Entities;

namespace EastRiders.BL.DAOs.CalendarDAO
{
    public class CalendarDAO : ICalendarRepository
    {
        private readonly DatabaseContext _context;

        public CalendarDAO(DatabaseContext context)
        {
            _context = context;
        }

        public int AddEvent(Calendar calendar)
        {
            _context.Calendar.Add(calendar);
            return _context.SaveChanges();
        }

        public IQueryable<Calendar> GetEvents()
        {
            return _context.Calendar.AsQueryable();
        }

        public int UpdateEvent(Calendar calendar)
        {
            _context.Entry(calendar).State = EntityState.Modified;
            return _context.SaveChanges();
        }

        public int DeleteEvent(int id)
        {
            Calendar calendar = new Calendar() { Id = id };
            _context.Entry(calendar).State = EntityState.Deleted;
            return _context.SaveChanges();
        }

        public void Dispose()
        {
            _context.Dispose();
        }
    }
}
