﻿using EastRiders.BL.Entities;
using System;
using System.Linq;

namespace EastRiders.BL.DAOs.CalendarDAO
{
    public interface ICalendarRepository : IDisposable
    {
        int AddEvent(Calendar calendar);
        IQueryable<Calendar> GetEvents();
        int UpdateEvent(Calendar news);
        int DeleteEvent(int id);
    }
}
