﻿using EastRiders.BL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace EastRiders.BL.DAOs.GalleryDAO
{
    public interface IGalleryRepository : IDisposable
    {
        int AddGallery(IEnumerable<Gallery> gallery);
        IEnumerable<Gallery> GetGalleryByLimit(int limit);
        IQueryable<Gallery> GetGalleries();
        void UpdateGallery(Gallery gallery);
        void DeleteGallery(int id);
    }
}
