﻿using System.Collections.Generic;
using System.Linq;
using EastRiders.BL.DatabaseLogic;
using EastRiders.BL.Entities;

namespace EastRiders.BL.DAOs.GalleryDAO
{
    public class GalleryDAO : IGalleryRepository
    {
        private readonly DatabaseContext _context;

        public GalleryDAO(DatabaseContext context)
        {
            _context = context;
        }

        public int AddGallery(IEnumerable<Gallery> gallery)
        {
            _context.Gallery.RemoveRange(_context.Gallery);
            _context.SaveChanges();
            _context.Gallery.AddRange(gallery);
            return _context.SaveChanges();
        }

        public void DeleteGallery(int id)
        {
            throw new System.NotImplementedException();
        }

        public IQueryable<Gallery> GetGalleries()
        {
            throw new System.NotImplementedException();
        }

        public IEnumerable<Gallery> GetGalleryByLimit(int limit)
        {
            var model = _context.Gallery.OrderByDescending(x => x.EventDate).Take(limit).ToList();
            return model;
        }

        public void UpdateGallery(Gallery gallery)
        {
            throw new System.NotImplementedException();
        }

        public void Dispose()
        {
            _context.Dispose();
        }
    }
}
