﻿using EastRiders.BL.Entities;
using System;
using System.Threading.Tasks;

namespace EastRiders.BL.DAOs.TokenDAO
{
    public interface ITokenRepository : IDisposable
    {
        Task<bool> AddRefreshToken(RefreshToken token);
        Task<RefreshToken> FindRefreshTokenAsync(string token);
        Task<bool> RemoveRefreshToken(string refreshToken);
        Task<bool> RemoveRefreshToken(RefreshToken refreshToken);
    }
}
