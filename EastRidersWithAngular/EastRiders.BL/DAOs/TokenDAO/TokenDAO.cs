﻿using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using EastRiders.BL.DatabaseLogic;
using EastRiders.BL.Entities;

namespace EastRiders.BL.DAOs.TokenDAO
{
    public class TokenDAO : ITokenRepository
    {
        private readonly DatabaseContext _context;

        public TokenDAO(DatabaseContext context)
        {
            _context = context;
        }

        public async Task<bool> AddRefreshToken(RefreshToken token)
        {

            var existingToken = _context.RefreshTokens.Where(r => r.UserId == token.UserId).SingleOrDefault();

            if (existingToken != null)
            {
                var result = await RemoveRefreshToken(existingToken);
            }

            _context.RefreshTokens.Add(token);

            return await _context.SaveChangesAsync() > 0;
        }

        public async Task<RefreshToken> FindRefreshTokenAsync(string token)
        {
            return await _context.RefreshTokens.SingleOrDefaultAsync(i => i.Token == token);
        }

        public async Task<bool> RemoveRefreshToken(string refreshToken)
        {
            var refreshTokenModel = await _context.RefreshTokens.SingleAsync(i => i.Token == refreshToken);

            if (refreshToken != null)
            {
                _context.RefreshTokens.Remove(refreshTokenModel);
                return await _context.SaveChangesAsync() > 0;
            }

            return false;
        }

        public async Task<bool> RemoveRefreshToken(RefreshToken refreshToken)
        {
            _context.RefreshTokens.Remove(refreshToken);
            return await _context.SaveChangesAsync() > 0;
        }

        public void Dispose()
        {
            _context.Dispose();
        }
    }
}
