﻿using EastRiders.BL.DatabaseLogic;

namespace EastRiders.BL.DAOs.AccountDAO
{
    public class AccountDAO : IAccountRepository
    {
        private readonly DatabaseContext _context;

        public AccountDAO(DatabaseContext context)
        {
            _context = context;
        }

        public void Dispose()
        {
            _context.Dispose();
        }
    }
}
