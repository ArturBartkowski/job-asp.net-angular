﻿using EastRiders.BL.Entities;
using System;
using System.Collections.Generic;

namespace EastRiders.BL.DAOs.NewsDAO
{
    public interface INewsRepository : IDisposable
    {
        int AddNews(News news);
        News GetNew(int id);
        List<News> GetNews();
        List<News> GetFirstsNewsByLimit(int limit);
        int UpdateNews(News news);
        int DeleteNews(int id);
    }
}
