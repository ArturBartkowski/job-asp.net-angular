﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using EastRiders.BL.DatabaseLogic;
using EastRiders.BL.Entities;

namespace EastRiders.BL.DAOs.NewsDAO
{
    public class NewsDAO : INewsRepository
    {
        private readonly DatabaseContext _context;

        public NewsDAO(DatabaseContext context)
        {
            _context = context;
        }

        public int AddNews(News news)
        {
            _context.News.Add(news);
            return _context.SaveChanges();
        }

        public int DeleteNews(int id)
        {
            News news = new News() { Id = id };
            _context.Entry(news).State = EntityState.Deleted;
            return _context.SaveChanges();
        }

        public List<News> GetFirstsNewsByLimit(int limit)
        {
            return _context.News.OrderByDescending(x => x.Id).Take(limit).ToList();
        }

        public News GetNew(int id)
        {
            return _context.News.Find(id);
        }

        public List<News> GetNews()
        {
            return _context.News.ToList();
        }

        public int UpdateNews(News news)
        {
            _context.Entry(news).State = EntityState.Modified;
            return _context.SaveChanges();
        }

        public void Dispose()
        {
            _context.Dispose();
        }
    }
}
